# This file contains many flags that are used either for the training of
# ptx2vec embedding space or for the final tasks

from absl import flags

#####################################
####### PTX2VEC FLAGS
#####################################
flags.DEFINE_string('ptx2vec_in_folder', 'ptx2vec_in',
	'Folder with the input data for ptx2vec training')
flags.DEFINE_string('ptx2vec_gen_files_folder', 'ptx2vec_gen_files',
 	'Folder that will contain files generated during the processing and training \
	of ptx2vec embedding space')
flags.DEFINE_string('ptx2vec_out_folder', 'ptx2vec_out',
	'Folder that will contain the resulting trained embeddings and vocabulary')

flags.DEFINE_enum('ptx2vec_mode', 'gen_train_data',
	['gen_train_data', 'train_skipgram'],
	'sets the mode for ptx2vec --  we can either generate \
	the data fro training of word embeddings with the Skip Gram model \
	or, if the data are already present, we can train the SkipGram model')
flags.DEFINE_bool('ptx2vec_restore', False, 
	'whether or not skip already processed files for generation of training data ... \
	if set to false, all of these files are removed at the start of the program, \
	if set to true, then the already processed files are skipped and the system continues \
	from the last checkpoint')
flags.DEFINE_enum('ptx2vec_ab_granularity',	'finest', ['finest'],
	'granularity for abstracting of instructions during generation \
	of training data for the Skip Gram model')

# flags related to generation of training data
flags.DEFINE_integer('ptx2vec_context_size', 2,
	'context size used during generating of training data for the skip-gram model')
flags.DEFINE_float('ptx2vec_subsampling_threshold',	1e-7,
	'threshold for subsampling (special technique related to training of the Skip-Gram model \
	that affects the number of training pairs)')
flags.DEFINE_integer('ptx2vec_cutoff', 300,
	'during the building of vocabulary, all statements that appear less than \
	cutoff times are replaced with <UNK> statement')

# flags related to the training of skip gram model
flags.DEFINE_integer('ptx2vec_batch_size', 64, 'skip-gram model batch size')
flags.DEFINE_integer('ptx2vec_epochs', 5, 'number of epochs')
flags.DEFINE_integer('ptx2vec_embedding_dim', 64, 'embedding dimensions')
flags.DEFINE_integer('ptx2vec_epoch_report_freq', 10, 'frequency of loss report during skip gram training')

#####################################
####### TASK FLAGS
#####################################

# for all tasks - folder with embeddings
flags.DEFINE_string('trained_emb', 'ptx2vec_out/ptx2vec_pretrained',
	'folder with pretrained ptx2vec embedding space for all tasks')

#####################################
####### HET. DEV. MAPPING TASK FLAGS (DEVMAP)
#####################################
flags.DEFINE_string('devmap', 'task_devmap', 'devmap task base folder')
flags.DEFINE_string('devmap_in' , 'task_devmap/in', 'devmap task input folder')
flags.DEFINE_string('devmap_gen_files', 'task_devmap/gen_files', 'devmap task gen files folder')
flags.DEFINE_string('devmap_out', 'task_devmap/out', 'devmap task out folder')

flags.DEFINE_integer('devmap_epochs', 50, 'number of training epochs')
flags.DEFINE_integer('devmap_batch_size', 64, 'batch size')
flags.DEFINE_integer('devmap_dense_layer_size', 32, 'size of dense layer')
flags.DEFINE_enum('devmap_benchmark', 'amd',
	['amd', 'nvidia'], 'which benchmark - amd or nvidia')
flags.DEFINE_bool('devmap_imm', False,
 	'whether to use models with or without immediate values')
flags.DEFINE_enum('devmap_approach', 'ptx2vec',
	['ptx2vec', 'ptx_ab'], 'which basic approach to use - ptx2vec or ptx_ab')
flags.DEFINE_enum('devmap_ab_granularity', 'coarse',
	['coarse'], 'used granularity in ptx_ab approach')
flags.DEFINE_integer('devmap_lstm_size', 64, 'size of lstm layer with ptx_ab')
flags.DEFINE_integer('devmap_emb_size', 64, 'size of embedding layer with ptx_ab models')
flags.DEFINE_integer('devmap_seed', 0, 'seed (0 = random)')

# only for testing purposes
flags.DEFINE_integer('devmap_split', 10, 'number of splits')
flags.DEFINE_integer('devmap_firstX', 0, 'take only first X number of examples')

#####################################
####### ACHIEVED OCCUPANCY TASK FLAGS (AOCC)
#####################################
flags.DEFINE_string('aocc', 'task_aocc', 'aocc task base folder')
flags.DEFINE_string('aocc_in', 'task_aocc/in', 'aocc task input folder')
flags.DEFINE_string('aocc_gen_files', 'task_aocc/gen_files', 'aocc task generated files folder')
flags.DEFINE_string('aocc_out', 'task_aocc/out', 'aocc task output folder')

flags.DEFINE_integer('aocc_epochs', 100, 'number of training epochs')
flags.DEFINE_integer('aocc_batch_size', 64, 'batch size')
flags.DEFINE_integer('aocc_dense_layer_size', 32, 'dense layer size')
flags.DEFINE_enum('aocc_approach', 'ptx2vec',
	['ptx2vec', 'ptx_ab'], 'which basic approach to use - ptx2vec or ptx_ab')
flags.DEFINE_enum('aocc_ab_granularity', 'coarse',
	['coarse'], 'used granularity in ptx_ab approach')
flags.DEFINE_integer('aocc_lstm_size', 64, 'size of lstm layer')
flags.DEFINE_integer('aocc_emb_size', 64, 'size of embedding layer')
flags.DEFINE_integer('aocc_seed', 0, 'seed (0 = random)')
flags.DEFINE_integer('aocc_class_count', 10, 'number of classes for classfication')

# regularization params
flags.DEFINE_float('aocc_dropout_rate', 0.7, 'dense layer dropout rate in aocc task')
flags.DEFINE_float('aocc_dense_regularization', 0.01, 'dense layer kernel regularization parameter for aocc_task')

# tensorboard logging parameters
# (tb_logs folder is saved under task_aocc folder)
flags.DEFINE_bool('aocc_tb_logging', False, 'whether to log for tensorboard or not')
flags.DEFINE_string('aocc_tb_folder', 'tb_logs', 'tensor board log dir')
flags.DEFINE_string('aocc_tb_model_name', None, 'tb log dir name for this model')

# only for testing purposes
flags.DEFINE_integer('aocc_split', 10, 'number of splits')
flags.DEFINE_integer('aocc_firstX', 0, 'take only first X number of examples')
