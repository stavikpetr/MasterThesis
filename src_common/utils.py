# This file contains various utility functions that 
# are used throughout the project.

import os
import shutil
import re
import pickle
import json

from typing import Dict
from typing import Tuple
from typing import Callable
from typing import List

from src_ptx2vec import ptx_info as ptx

def find_all_extract_last(pattern : str, text : str) -> str:
	# in the @text, finds all occurences of the matching pattern
	# and returns the last matching pattern ...
	# note however that this functions requires only one matching group
	# otherwise, it will return a tuple
	all_matches = re.findall(pattern, text)
	last_match = all_matches[len(all_matches) - 1]

	return last_match

class ptx2vecException(Exception):
	pass

def error(msg):
	print('******')
	print('*** ERROR ENCOUNTERED')
	print('******')
	raise ptx2vecException(msg)

def warning(msg):
	print('******')
	print('*** WARNING: ' + msg)
	print('******')


def remove_all_folders(folder):
	for f in os.listdir(folder):
		if os.path.isdir(os.path.join(folder, f)):
			shutil.rmtree(os.path.join(folder, f))

#####################################
####### WORKING WITH PTX FUNCTIONS
#####################################

def find_enclosing_bracket(lines : list, start_idx : int) -> int:
	# this function finds the enclosing bracket in a block of
	# code enclosed in braces
	counter = 0

	for i in range(start_idx, len(lines)):
		line = lines[i]
		if line == '{':
			counter += 1
		elif line == '}':
			counter -= 1
		
		if counter == 0:
			return i

	error('can not find enclosing bracket')

def collect_functions(ptx_file_lines : list) -> List[Tuple[str, str, list, int]]:
	# this function can be used to collect functions of 
	# ptx file ... it returns a list of tuples with function definition line,
	# then function body lines and lastly, the index of the first 
	# function body line (this can be useful for replacement)
	functions = []

	idx = 0
	while idx < len(ptx_file_lines):
		line = ptx_file_lines[idx]
		if re.match(ptx.r_oneline_defined_func, line) or re.match(ptx.r_oneline_kernel, line):
			if ptx_file_lines[idx + 1] != '{':
				print(ptx_file_lines[idx - 1])
				print(ptx_file_lines[idx])
				print(ptx_file_lines[idx + 1])
				error('can not collection functions ... idx + 1 != {')
			# idx + 1 contains the starting bracket '{'
			enclosing_idx = find_enclosing_bracket(ptx_file_lines, idx + 1)
			f_def_line = line
			f_name = find_all_extract_last(ptx.r_func_identifier, f_def_line)
			f_body_lines = ptx_file_lines[idx + 2 : enclosing_idx]

			functions.append((f_def_line, f_name, f_body_lines, idx+2))

			idx = enclosing_idx + 1
		else:
			idx += 1

	return functions


def transform_function_body(f_body_lines : list, func : Callable[[str], str]) -> None:
	# transforms function body by applying @func to each line
	# and substituting it
	idx = 0
	while idx < len(f_body_lines):
		f_body_line = f_body_lines[idx]

		transformed_line = func(f_body_line)

		f_body_lines[idx] = transformed_line

		idx += 1

def transform_function_bodies(ptx_file_lines : list, func : Callable[[str], str]) -> list:
	# transforms bodies of all functions by applying @func
	# to each line and substituting it
	functions = collect_functions(ptx_file_lines)

	for _, _, f_body_lines, f_body_start_idx in functions:

		transform_function_body(f_body_lines, func)
		
		ptx_file_lines[f_body_start_idx:f_body_start_idx + len(f_body_lines)] = f_body_lines

	return ptx_file_lines

#####################################
####### ITERATING OVER FILES
#####################################

def find_files_by_extension(directory : str, extension : str) -> Dict[str, list] :
	# this function returns all files in the second level 
	# of the passed dir with the provided extension
	# (used e.g. while collecting data files for preprocessing)
	# this function return dictionary { dir : list-of-files }
	ptx_dict = {}

	for entry in os.listdir(directory):
		entry_path = os.path.join(directory, entry)
		if os.path.isdir(entry_path):
			for n_entry in os.listdir(entry_path):
				if n_entry.endswith(extension):
					if not(ptx_dict.get(entry)):
						ptx_dict[entry] = list()
					ptx_dict[entry].append(n_entry)
				
	return ptx_dict

def read_files(read_dir, read_ext, reader):
	# this function returns an iterator with all files with the @read_ext extension
	# under the read_dir that are read with the @reader func
	files_dict = find_files_by_extension(read_dir, read_ext)

	if len(files_dict) == 0:
		error('can not continue with phase ... no input files were found')

	dir_count = len(files_dict)
	dir_counter = 1

	for dir_name, files in files_dict.items():
		print(f'\tdir {dir_name} ({dir_counter} out of {dir_count}) ... ')
		dir_path = os.path.join(read_dir, dir_name)
		f_count = len(files)
		f_counter = 1
		for f_name in files:
			print(f'\t\tfile {f_name} ({f_counter} out of {f_count}) ... ')
			f_counter += 1
			f_path = os.path.join(dir_path, f_name)
			yield (dir_name, f_name, reader(f_path))
		dir_counter += 1

#####################################
####### I/O
#####################################

# text files
def read_text_f(file_path :str):
	with open(file_path, 'r') as f:
		file_content = f.read()
	return file_content

def append_text_f(file_path : str, data : str):
	with open(file_path, 'a') as f:
		f.write(data)

def write_text_f(file_path : str, data : str):
	with open(file_path, 'w') as f:
		f.write(data)

# json
def write_json(file_path : str, data : dict, indent = 4, sort_keys = True):
	with open(file_path, 'w') as f:
		json.dump(data, f, indent=indent, sort_keys=sort_keys)

def read_json(file_path : str):
	with open(file_path, 'r') as f:
		return json.load(f)

# binary serialization / deserialization with pickle
def pickle_generic_w(file_path : str, data):
	with open(file_path, 'wb') as f:
		f.write(pickle.dumps(data))

def pickle_generic_r(file_path : str):
	with open(file_path, 'rb') as f:
		data = pickle.load(f)
	return data

def pickle_xfg_w(file_path : str, g_context, f_context, xfg):
	with open(file_path, 'wb') as f:
		f.write(pickle.dumps(g_context))
		f.write(pickle.dumps(f_context))
		f.write(pickle.dumps(xfg))

def pickle_xfg_r(file_path : str):
	with open(file_path, 'rb') as f:
		g_context = pickle.load(f)
		f_context = pickle.load(f)
		xfg = pickle.load(f)

	return (g_context, f_context, xfg)

#####################################
####### REPORTING RELATED
#####################################

def report_phase_started(phase_name):
	print('******************')
	print('***** PHASE: ' + phase_name)
	print('******************')

def report_file_stats(files_dict):
	num_dirs = len(files_dict)
	num_files = sum(len(value) for key, value in files_dict.items())
	print('\tprocessing ' + str(num_dirs) + ' directories with ' + str(num_files) + ' files')

def report_program_start(mode):
	print('--------------')
	print('---- STARTING THE PROGRAM IN MODE: ' + mode)
	print('--------------')

def report_xfg_building(num, count):
	print(f'\t\t\tbuilding xfg for function ... ({num} out of {count})')

def report_xfg_building_progress(line_num, lines_total):
	print(f'\t\t\t\t{line_num} lines processed out of {lines_total}')

def report_epoch_started(num, count):
	print('\t-----')
	print(f'\t----- EPOCH {num} STARTED (out of {count})')
	print('\t-----')

def report_epoch_ended(avg_loss):
	print('')
	print(f'\t----- epoch avg loss: {avg_loss}')
	print('')

def report_batch_loss(step, batch_loss):
	print(f'\t\tstep {step} ... batch_loss: {batch_loss}')

def report_restore_flag(restore):
	msg = 'restore flag is set to: ' + str(restore)
	if restore:
		msg += ' ... any files that were processed are skipped'
	else:
		msg += ' ... any generated files are removed'
	print(msg)

