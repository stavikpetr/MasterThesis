# This file contains several utility functions that are
# used in both devmap and aocc tasks.
import os
from shutil import copyfile
import numpy as np

from src_common import utils

def idx_to_one_hot(vocab_len, idx):
	one_hot = [0] * vocab_len
	one_hot[idx] = 1
	return one_hot

#####################################
####### ptx2vec
#####################################

def copy_ptx2vec_related_files(res_folder_path, emb_folder):
	# this function copies various files related to the ptx2vec
	# approach to the folder with task results
	copyfile(os.path.join(emb_folder, 'vocab_idx.json'), os.path.join(res_folder_path, 'vocab_idx.json'))
	copyfile(os.path.join(emb_folder, 'vocab_freq.json'), os.path.join(res_folder_path, 'vocab_freq.json'))

	if os.path.exists(os.path.join(emb_folder, 'vocab_coff.json')):
		copyfile(os.path.join(emb_folder, 'vocab_coff.json'), os.path.join(res_folder_path, 'vocab_coff.json'))
	
	copyfile(os.path.join(emb_folder, 'emb_stats.json'), os.path.join(res_folder_path, 'emb_stats.json'))
	copyfile(os.path.join(emb_folder, 'emb_matrix.p'), os.path.join(res_folder_path, 'emb_matrix.p'))		


def ptx2vec_load_emb_files(emb_folder):
	# this function loads various files used in the ptx2vec approach
	# most importantly, it loads the embedding matrix 
	# and the vocabulary for encoding 

	emb_matrix_p = os.path.join(emb_folder, 'emb_matrix.p')
	vocab_idx_p = os.path.join(emb_folder, 'vocab_idx.json')
	emb_stats_p = os.path.join(emb_folder, 'emb_stats.json')

	if not os.path.exists(emb_matrix_p) or not os.path.exists(vocab_idx_p) \
		or not os.path.exists(emb_stats_p):
		utils.error('can not train dev_map task - can not find emb_matrix, vocab_idx or emb_stats')
	
	emb_matrix = utils.pickle_generic_r(emb_matrix_p)
	vocab_idx = utils.read_json(vocab_idx_p)
	emb_stats = utils.read_json(emb_stats_p)

	if not len(vocab_idx['vocab_idx']) == emb_matrix.shape[0]:
		utils.error('can not train dev_map task - dimensions of vocabulary and embedding matrix do not match')

	emb_matrix = emb_matrix
	vocab_idx = vocab_idx
	emb_stats = emb_stats

	return emb_matrix, vocab_idx, emb_stats


def kernel_line_to_one_hot(k_line, vocab_idx, unk_stmt_idx):
	# maps single kernel line to either vocabulary instruction
	# or an unkown statement if it is not present there
	if k_line in vocab_idx:
		k_line_idx = vocab_idx[k_line]
	else:
		k_line_idx = unk_stmt_idx

	k_line_one_hot = idx_to_one_hot(len(vocab_idx), k_line_idx)
	return k_line_one_hot

def ptx2vec_kernel_to_emb(kernel_lines, emb_matrix, vocab_idx):
	# encodes a single kernel as a list of embedding vectors
	kernel_emb = []

	for k_line in kernel_lines:
		k_line_one_hot = kernel_line_to_one_hot(k_line, vocab_idx['vocab_idx'], vocab_idx['unk_stmt_idx'])
		k_line_emb = np.matmul(k_line_one_hot, emb_matrix)
		kernel_emb.append(k_line_emb)

	return kernel_emb

def ptx2vec_kernels_to_emb(gen_files_folder, emb_matrix, vocab_idx):
	# This function encodes the kernels for ptx2vec approach
	# into embedding vectors

	kernels_emb = {}
	maxlen = 0

	# go over each abstracted task input file
	for _, file_name, kernel_ab in utils.read_files(gen_files_folder, '_ab.txt', utils.read_text_f):
		kernel_lines = kernel_ab.splitlines()

		# and abstract it
		kernel_emb = ptx2vec_kernel_to_emb(kernel_lines, emb_matrix, vocab_idx)

		kernels_emb[file_name[:-7]] = kernel_emb

		maxlen = len(kernel_emb) if len(kernel_emb) > maxlen else maxlen
	
	return kernels_emb, maxlen

def ptx2vec_kernels_to_padded_emb(kernels_emb, maxlen, emb_dim):
	# pads each kernel encoded for ptx2vec approach 
	# to the same length (this is required by LSTM layer)
	empty = [0.] * emb_dim

	for kernel_id, kernel_lines in kernels_emb.items():
		while len(kernel_lines) != maxlen:
			kernel_lines.append(empty)

	return kernels_emb

#####################################
####### ptx_ab
#####################################

def ptx_ab_create_vocabulary(gen_files_folder):
	# This function creates the vocabulary for ptx_ab approach
	#
	# in ptx_ab, we use embedding layer that mask zero values of 
	# padded kernels ... however, with this apporach, it is not possible
	# to use 0 as an index and we have to start from index 1
	vocab_idx = {}
	for _, file_name, kernel_ab in utils.read_files(gen_files_folder, '_ab.txt', utils.read_text_f):
		kernel_lines = kernel_ab.splitlines()
		for k_line in kernel_lines:
			if k_line not in vocab_idx:
				vocab_idx[k_line] = len(vocab_idx) + 1
	return vocab_idx

def ptx_ab_emb_kernel_to_ind(kernel_lines, vocab_idx):
	# this function encodes a single kernel as a vector
	# of vocabulary indices
	kernel_ind = []
	for k_line in kernel_lines:
		kernel_ind.append(vocab_idx[k_line])
	return kernel_ind

def ptx_ab_emb_kernels_to_indices(gen_files_folder, vocab_idx):
	# This function encodes the kernels for ptx_ab approach
	# into one-hot encoding based on the provided vocabulary
	#
	# it is important to note, that when we use keras embedding layer,
	# each kernel needs to represented as a vector of indices to vocabulary
	# this is contrary to the ptx2vec approach where the kernels consist
	# of embedding vectors
	kernels_ind = {}
	maxlen = 0

	# go over each abstracted task input file
	for _, file_name, kernel_ab in utils.read_files(gen_files_folder, '_ab.txt', utils.read_text_f):
		kernel_lines = kernel_ab.splitlines()

		# and encode it
		kernel_ind = ptx_ab_emb_kernel_to_ind(kernel_lines, vocab_idx)
		kernels_ind[file_name[:-7]] = kernel_ind

		maxlen = len(kernel_ind) if len(kernel_ind) > maxlen else maxlen

	return kernels_ind, maxlen

def ptx_ab_emb_kernels_padded(kernels_ind, maxlen):
	# pads each kernel encoded for ptx_ab approach 
	# to the same length (this is required by LSTM layer)

	for kernel_id, kernel_lines in kernels_ind.items():
		while len(kernel_lines) != maxlen:
			kernel_lines.append(0)

	return kernels_ind