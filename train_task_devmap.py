# This file tests our two approaches - ptx2vec and ptx_ab
# in the hetergoeneous device mapping task.

import os
import numpy as np
import pandas as pd
from shutil import copyfile
from typing import Optional
from dataclasses import dataclass
from absl import flags, app

from tensorflow.keras.layers import Input, Embedding, LSTM, Dense, Dropout
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.models import Model
from sklearn.model_selection import StratifiedKFold
from tensorflow.keras.layers import Masking

from src_common import appflags
from src_common import utils
from src_common import task_utils
from src_ptx2vec import preprocesser
from src_ptx2vec import xfg_abstracter


FLAGS=flags.FLAGS

# common settings for both ptx2vec and ptx_ab approaches
# for the device mapping task
@dataclass
class Devmap_settings:
	approach: str
	base_folder: str
	in_folder: str
	gen_files_folder: str
	out_folder: str
	benchmark: str
	imm: bool
	epochs: int
	batch_size: int
	dense_layer_size: int
	seed: int
	split: int # for testing purposes
	firstX: int # for testing purposes
	emb_folder: Optional[str] # parameter for ptx2vec
	ab_granularity: Optional[str] # parameter for ptx_ab
	lstm_size: Optional[int] # parameter for ptx_ab
	emb_size: Optional[int] # parameter for ptx_ab

#####################################
####### COMMON INIT FUNCTIONS
#####################################

def get_df(base_folder, benchmark):
	# reads the data for the benchmark into a pandas dataframe
	df = pd.read_csv(os.path.join(base_folder, f'cgo17-{benchmark}.csv'))
	return df
	
def get_X_Y(df, kernels_enc):
	# create training dataset out of the encoded kernels
	# and information in the loaded dataframe
	X = []
	Y = []
	Y_one_hot = []

	for _, row in df.iterrows():
		kernel_id = row['benchmark']
		if kernel_id[:3] == 'npb':
			kernel_id += '_'
			kernel_id += row['dataset']
		# this check is only for testing with smaller datasets
		if not kernel_id in kernels_enc:
			continue
		X.append(kernels_enc[kernel_id])
		Y.append(1 if row['oracle'] == 'GPU' else 0)
		Y_one_hot.append([0, 1] if row['oracle'] == 'GPU' else [1, 0])

	# keras does not like if the inputs are not numpy arrays
	return np.asarray(X), np.asarray(Y), np.asarray(Y_one_hot)

def get_aux_inputs(df):
	# get auxiliary inputs from the dataframe
	return np.array([
		df['transfer'].values,
		df['wgsize'].values
	]).T

#####################################
####### ptx2vec
#####################################
# first model for ptx2vec - without immediate values
class Model_ptx2vec:
	name = 'ptx2vec'

	def init(self, seed, maxlen, emb_dim, dense_layer_size):
		np.random.seed(seed)

		empty = [0.] * emb_dim

		input_layer = Input(shape=(maxlen, emb_dim), dtype='float32', name='input_layer')
		masking_layer = Masking(mask_value=empty)(input_layer)
		x = LSTM(emb_dim, implementation=1, return_sequences=True, name='lstm_1')(masking_layer)
		x = LSTM(emb_dim, implementation=1, name='lstm_2')(x)
		
		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu')(x)
		out = Dense(2, activation='sigmoid', name='actual_out')(x)

		self.model = Model(inputs=input_layer, outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])


	def train(self, X, Y, epochs, batch_size):
		self.model.fit(x=X, y=Y, epochs=epochs, shuffle=True, batch_size=batch_size)

	def predict(self, X):
		predictions = np.array(self.model.predict(X))
		predictions_arg_max = [np.argmax(p) for p in predictions]
		return predictions_arg_max

# second model for ptx2vec - with immediate values
class Model_ptx2vec_imm:
	name = 'ptx2vec-imm'

	def init(self, seed, maxlen, emb_dim, dense_layer_size):
		np.random.seed(seed)

		empty = [0.] * emb_dim

		input_layer = Input(shape=(maxlen, emb_dim), dtype='float32', name='code_in')
		masking_layer = Masking(mask_value=empty)(input_layer)
		x = LSTM(emb_dim, implementation=1, return_sequences=True, name='lstm_1')(masking_layer)
		x = LSTM(emb_dim, implementation=1, name='lstm_2')(x)

		auxiliary_inputs = Input(shape=(2,))
		x = Concatenate()([auxiliary_inputs, x])
		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu')(x)
		out = Dense(2, activation='sigmoid')(x)

		self.model = Model(inputs=[input_layer, auxiliary_inputs], outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])


	def train(self, X, aux_inputs, Y, epochs, batch_size):
		self.model.fit(x=[X, aux_inputs], y=Y, epochs=epochs, shuffle=True, batch_size=batch_size)

	def predict(self, X, aux_inputs):
		predictions = np.array(self.model.predict([X, aux_inputs]))
		predictions_arg_max = [np.argmax(p) for p in predictions]
		return predictions_arg_max

def ptx2vec_eval(settings : Devmap_settings):
	# function that initializes one of the ptx2vec models,
	# prepares kernels for training and then invokes the eval_common
	# function that actually performs the training and evaluation

	# with ptx2vec approach, we first need to load the embedding matrix
	# and vocabulary
	emb_matrix, vocab_idx, emb_stats = task_utils.ptx2vec_load_emb_files(settings.emb_folder)

	# now, preprocess and abstract all of the task files that will be used
	# for training/evaluation
	preprocesser.preprocess(settings.in_folder, settings.gen_files_folder, False)
	xfg_abstracter.abstract_ptxs(settings.gen_files_folder, vocab_idx['ab_granularity'])

	# now, load the abstracted files and encode them
	kernels_emb, maxlen = task_utils.ptx2vec_kernels_to_emb(settings.gen_files_folder, emb_matrix, vocab_idx)
	# pad the kernels to same length
	padded_kernels_emb = task_utils.ptx2vec_kernels_to_padded_emb(kernels_emb, maxlen, emb_stats['emb_dim'])

	# create model
	if settings.imm == False:
		model = Model_ptx2vec()
	else:
		model = Model_ptx2vec_imm()

	# and perform the training and evaluation
	return eval_common(settings, model, padded_kernels_emb, maxlen, emb_stats['emb_dim'])


#####################################
####### ptx_ab
#####################################

# first model for ptx_ab - without immediate values
class Model_ptx_ab:
	name = 'ptx_ab'

	def init(self, seed, maxlen, vocab_size, dense_layer_size):
		np.random.seed(seed)

		input_layer = Input(shape=(maxlen,), name='input_layer')
		emb = Embedding(input_dim=vocab_size + 1, input_length=maxlen, output_dim=self.emb_layer_size, mask_zero=True)(input_layer)
		x = LSTM(self.lstm_size, implementation=1, return_sequences=True, name='lstm_1')(emb)
		x = LSTM(self.lstm_size, implementation=1, name='lstm_2')(x)

		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu')(x)
		out = Dense(2, activation='sigmoid', name='actual_out')(x)

		self.model = Model(inputs=input_layer, outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])

	def train(self, X, Y, epochs, batch_size):
		self.model.fit(x=X, y=Y, epochs=epochs, shuffle=True, batch_size=batch_size)

	def predict(self, X):
		predictions = np.array(self.model.predict(X))
		predictions_arg_max = [np.argmax(p) for p in predictions]
		return predictions_arg_max

	def __init__(self, lstm_size, emb_layer_size):
		self.lstm_size = lstm_size
		self.emb_layer_size = emb_layer_size

# second model for ptx_ab approach - with immediate values
class Model_ptx_ab_imm:
	name = 'ptx_ab-imm'

	def init(self, seed, maxlen, vocab_size, dense_layer_size):
		np.random.seed(seed)

		input_layer = Input(shape=(maxlen,), name='input_layer')
		emb = Embedding(input_dim=vocab_size + 1, input_length=maxlen, output_dim=self.emb_layer_size, mask_zero=True)(input_layer)
		x = LSTM(self.lstm_size, implementation=1, return_sequences=True, name='lstm_1')(emb)
		x = LSTM(self.lstm_size, implementation=1, name='lstm_2')(x)
		
		auxiliary_inputs = Input(shape=(2,))
		x = Concatenate()([auxiliary_inputs, x])
		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu')(x)
		out = Dense(2, activation='sigmoid')(x)

		self.model = Model(inputs=[input_layer, auxiliary_inputs], outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])


	def train(self, X, aux_inputs, Y, epochs, batch_size):
		self.model.fit(x=[X, aux_inputs], y=Y, epochs=epochs, shuffle=True, batch_size=batch_size)

	def predict(self, X, aux_inputs):
		predictions = np.array(self.model.predict([X, aux_inputs]))
		predictions_arg_max = [np.argmax(p) for p in predictions]
		return predictions_arg_max

	def __init__(self, lstm_size, emb_layer_size):
		self.lstm_size = lstm_size
		self.emb_layer_size = emb_layer_size

def ptx_ab_eval(settings: Devmap_settings):
	# function that initializes one of the ptx_ab models,
	# prepares kernels training and then invokes the eval_common
	# function that actually performs the training and evaluation

	ab_granularity = settings.ab_granularity
	
	# perform the preprocessing and abstraction of the task input files
	preprocesser.preprocess(settings.in_folder, settings.gen_files_folder, False)
	xfg_abstracter.abstract_ptxs(settings.gen_files_folder, ab_granularity)

	# now, contrary to the ptx2vec approach, we need to create the vocabulary
	# as there is no vocabulary that was produced beforehand for this approach
	vocab_idx = task_utils.ptx_ab_create_vocabulary(settings.gen_files_folder)
	# encode the kernels of the task input files
	kernels_ind, maxlen = task_utils.ptx_ab_emb_kernels_to_indices(settings.gen_files_folder, vocab_idx)
	# pad the kernels to the same length
	kernels_ind_padded = task_utils.ptx_ab_emb_kernels_padded(kernels_ind, maxlen)

	# create model
	if settings.imm == False:
		model = Model_ptx_ab(settings.lstm_size, settings.emb_size)
	else:
		model = Model_ptx_ab_imm(settings.lstm_size, settings.emb_size)

	# and perform the training and evaluation
	return vocab_idx, eval_common(settings, model, kernels_ind_padded, maxlen, len(vocab_idx))
	
#####################################
####### TRAINING AND EVAL
#####################################
def eval_common(settings: Devmap_settings, model, padded_kernels_enc: dict, maxlen, model_shape_param):
	# get the training data
	df = get_df(settings.base_folder, settings.benchmark)
	X, Y, Y_one_hot = get_X_Y(df, padded_kernels_enc)
	aux_inputs = get_aux_inputs(df)

	# only for debugging
	if settings.firstX != 0:
		X = X[:settings.firstX]
		Y = Y[:settings.firstX]
		Y_one_hot = Y_one_hot[:settings.firstX]
		aux_inputs = aux_inputs[:settings.firstX]

	results = []

	# use stratified n-fold cross-validation
	kf = StratifiedKFold(n_splits=settings.split, shuffle=True, random_state=settings.seed)
	for i, (train_indices, test_indices) in enumerate(kf.split(X, Y)):
		print(f'**** benchmark {settings.benchmark}, model {model.name}, split {i} (out of {settings.split}) ****')
		
		# training data for this split
		X_train = X[train_indices]
		Y_one_hot_train = Y_one_hot[train_indices]
		aux_train = aux_inputs[train_indices]

		# testing data for this split
		X_test = X[test_indices]
		Y_test = Y[test_indices]
		aux_test = aux_inputs[test_indices]

		# initialize model
		model.init(settings.seed, maxlen, model_shape_param, settings.dense_layer_size)

		# perform the whole training
		for i in range(settings.epochs):
			print(f'epoch {i+1} / {settings.epochs}')
			if not 'imm' in model.name:
				model.train(X_train, Y_one_hot_train, 1, settings.batch_size)
			else:
				model.train(X_train, aux_train, Y_one_hot_train, 1, settings.batch_size)

			if (i + 1) % 5 == 0:
				if not 'imm' in model.name:
					Y_predictions = model.predict(X_test)
				else:
					Y_predictions = model.predict(X_test, aux_test) 
				correct = Y_test == Y_predictions
				test_acc = sum(correct) / len(correct)
				print(f'\t\ttest accuracy: {test_acc}')

		# make the predictions for the testing dataset
		if not 'imm' in model.name:
			Y_predictions = model.predict(X_test)
		else:
			Y_predictions = model.predict(X_test, aux_test) 

		# and append the results into the final dataframe
		correct = Y_test == Y_predictions
		
		if settings.benchmark == 'amd':
			b_runtimes = df['runtime_cpu'][test_indices]
		else:
			b_runtimes = df['runtime_gpu'][test_indices]

		cpu_gpu_runtimes = df[['runtime_cpu', 'runtime_gpu']].values[test_indices]
		p_runtimes = np.array([row[y_prediction] for y_prediction, row in zip(Y_predictions, cpu_gpu_runtimes)])
		speedups = b_runtimes / p_runtimes

		for y_test, y_pred, c_val, b_runtime, p_runtime, s_val in zip(Y_test, Y_predictions, correct, b_runtimes, p_runtimes, speedups):
			results.append({
				'approach' : settings.approach,
				'benchmark' : settings.benchmark,
				'model': model.name,
				'b_chosen': 'CPU' if settings.benchmark == 'amd' else 'GPU',
				'Y_actual' : 'GPU' if y_test else 'CPU',
				'Y_pred' : 'GPU' if y_pred else 'CPU',
				'Correct?' : c_val,
				'b_runtime' : b_runtime,
				'p_runtime' : p_runtime,
				'speedup' : s_val
			})

	return pd.DataFrame(results, columns = [
		'approach',
		'benchmark',
		'model',
		'b_chosen',
		'Y_actual',
		'Y_pred',
		'Correct?',
		'b_runtime',
		'p_runtime',
		'speedup'
	])

#####################################
####### MAIN FUNCTION
#####################################
def main(argv):
	# for arguments, we are using the absl library, the original flags are ommited
	del argv

	# folders
	devmap_folder = FLAGS.devmap
	in_folder = FLAGS.devmap_in
	gen_files_folder = FLAGS.devmap_gen_files
	out_folder = FLAGS.devmap_out

	# which benchmark (AMD x NVIDA), and model to use
	benchmark = FLAGS.devmap_benchmark
	imm = FLAGS.devmap_imm
	approach = FLAGS.devmap_approach

	# training settings
	epochs = FLAGS.devmap_epochs
	batch_size = FLAGS.devmap_batch_size
	dense_layer_size = FLAGS.devmap_dense_layer_size
	seed = FLAGS.devmap_seed if FLAGS.devmap_seed != 0 else np.random.randint(1000)
	split = FLAGS.devmap_split
	firstX = FLAGS.devmap_firstX

	# create the settings 
	settings = Devmap_settings(
		approach,
		devmap_folder,
		in_folder,
		gen_files_folder,
		out_folder,
		benchmark,
		imm,
		epochs,
		batch_size,
		dense_layer_size,
		seed,
		split,
		firstX,
		None,
		None,
		None,
		None
	)

	# perform the training and get the results in a dataframe
	if approach == 'ptx2vec':
		settings.emb_folder = FLAGS.trained_emb
		df = ptx2vec_eval(settings)
	elif approach == 'ptx_ab':
		settings.ab_granularity = FLAGS.devmap_ab_granularity
		settings.lstm_size = FLAGS.devmap_lstm_size
		settings.emb_size = FLAGS.devmap_emb_size
		vocab_idx, df = ptx_ab_eval(settings)

	# comparison with other works that already addressed this task
	print('\n**** predictions ****')
	print(df.groupby(['model', 'benchmark'])['Correct?', 'speedup'].mean())

	if benchmark == 'amd':
		static_pred_vals = 58.823529
		static_sp_vals = 1.0
		grewe_pred_vals = 73.382353
		grewe_sp_vals = 2.905822
		deeptune_pred_vals = 83.676471
		deeptune_sp_vals = 3.335612
		inst2vec_pred_vals = 82.79
		inst2vec_sp_vals = 3.42
		inst2vecimm_pred_vals = 88.09
		inst2vecimm_sp_vals = 3.47
	elif benchmark == 'nvidia':
		static_pred_vals = 56.911765
		static_sp_vals = 1.0
		grewe_pred_vals = 72.941176
		grewe_sp_vals = 1.264801
		deeptune_pred_vals = 80.294118
		deeptune_sp_vals = 1.412222
		inst2vec_pred_vals = 82.06
		inst2vec_sp_vals = 1.42
		inst2vecimm_pred_vals = 86.62
		inst2vecimm_sp_vals = 1.44

	# accuracy comparison
	d = []
	d.append(static_pred_vals)
	d.append(grewe_pred_vals)
	d.append(deeptune_pred_vals)
	d.append(inst2vec_pred_vals)
	d.append(inst2vecimm_pred_vals)
	d.append(df['Correct?'].mean()* 100)

	d = np.array(d).T.reshape(1, 6)
	df_acc = pd.DataFrame(d, columns=['Static mapping', 'Grewe et al.', 'DeepTune', 'inst2vec',
									 'inst2vec-imm', df['model'][0]], index=[benchmark.upper()])
	
	print('\n**** accuracy comparisons ****')
	print(df_acc)

	# speedup comparison
	d = []
	d.append(static_sp_vals)
	d.append(grewe_sp_vals)
	d.append(deeptune_sp_vals)
	d.append(inst2vec_sp_vals)
	d.append(inst2vecimm_sp_vals)
	d.append(df['speedup'].mean())

	d = np.array(d).T.reshape(1, 6)
	df_speedup = pd.DataFrame(d, columns=['Static mapping', 'Grewe et al.', 'DeepTune', 'inst2vec',
									 'inst2vec-imm', df['model'][0]], index=[benchmark.upper()])
	
	print('\n**** speedup comparisons ****')
	print(df_speedup)

	# writing down everything to files ...
	# detect the number of folders in the out folder (the resulting
	# folder will then be "res_<len>")
	res_num = len([i for i in os.listdir(os.path.join(out_folder, '')) if os.path.isdir(os.path.join(out_folder, i))])
	res_folder_name = f'res_{res_num}'
	res_folder_path = os.path.join(out_folder, res_folder_name)
	os.mkdir(res_folder_path)

	# if we use ptx2vec approach, copy some of the related files to the
	# output folder
	if approach == 'ptx2vec':
		task_utils.copy_ptx2vec_related_files(res_folder_path, settings.emb_folder)

	# now, we want to output all of the dataframes
	df.to_csv(os.path.join(res_folder_path, 'results.csv'), sep='\t', index=False)
	df_acc.to_csv(os.path.join(res_folder_path, 'accuracies.csv'), sep='\t', index=True)
	df_speedup.to_csv(os.path.join(res_folder_path, 'speedups.csv'), sep='\t', index=True)

	# also, report the overall settings
	exp_settings = {
		'approach' : approach,
		'benchmark' : benchmark,
		'imm' : imm,
		'epochs': epochs,
		'batch_size': batch_size,
		'dense_layer_size': dense_layer_size,
		'seed': seed
	}

	# if we use ptx_ab approach, output some additional settings
	if approach == 'ptx_ab':
		exp_settings['ab_granularity'] = settings.ab_granularity
		exp_settings['lstm_size'] = settings.lstm_size
		exp_settings['emb_size'] = settings.emb_size
		exp_settings['vocab_idx'] = vocab_idx

	settings_path = os.path.join(res_folder_path, 'settings.json')
	utils.write_json(settings_path, exp_settings)

if __name__ == '__main__':
	app.run(main)