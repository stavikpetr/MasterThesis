# This file tests our two approaches - ptx2vec and ptx_ab
# in the achieved occupancy task.

import os
import numpy as np
import pandas as pd
from shutil import copyfile
from absl import flags, app
from typing import Optional
from dataclasses import dataclass
from datetime import datetime

from tensorflow.keras.layers import Input, Embedding, LSTM, Dense, Dropout
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.models import Model
from sklearn.model_selection import StratifiedKFold, KFold
from tensorflow.keras.layers import Masking
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import Callback
from tensorflow.keras import regularizers
import tensorflow as tf

from src_common import appflags
from src_common import utils
from src_common import task_utils
from src_ptx2vec import preprocesser
from src_ptx2vec import xfg_abstracter


FLAGS=flags.FLAGS

# callback class that is used for tensorboard logging
# taken from https://stackoverflow.com/questions/47877475/keras-tensorboard-plot-train-and-validation-scalars-in-a-same-figure
class TrainValTensorBoard(TensorBoard):
	def __init__(self, log_dir='./logs', **kwargs):
		# Make the original `TensorBoard` log to a subdirectory 'training'
		training_log_dir = os.path.join(log_dir, 'training')
		super(TrainValTensorBoard, self).__init__(training_log_dir, **kwargs)	
		# Log the validation metrics to a separate subdirectory
		self.val_log_dir = os.path.join(log_dir, 'validation')	
	def set_model(self, model):
		# Setup writer for validation metrics
		self.val_writer = tf.summary.FileWriter(self.val_log_dir)
		super(TrainValTensorBoard, self).set_model(model)	
	def on_epoch_end(self, epoch, logs=None):
		# Pop the validation logs and handle them separately with
		# `self.val_writer`. Also rename the keys so that they can
		# be plotted on the same figure with the training metrics
		logs = logs or {}
		#rint(logs.keys())
		val_logs = {k.replace('val_', 'epoch_'): v for k, v in logs.items() if k.startswith('val_')}
		for name, value in val_logs.items():
			summary = tf.Summary()
			summary_value = summary.value.add()
			summary_value.simple_value = value.item()
			summary_value.tag = name
			self.val_writer.add_summary(summary, epoch)
		self.val_writer.flush()
		self.writer.flush()
		# Pass the remaining logs to `TensorBoard.on_epoch_end`
		logs = {k: v for k, v in logs.items() if not k.startswith('val_')}
		super(TrainValTensorBoard, self).on_epoch_end(epoch, logs)

	def on_train_end(self, logs=None):
		super(TrainValTensorBoard, self).on_train_end(logs)
		self.val_writer.close()

# callback that reports the x-bin accuracy metric after
# every fifth epoch
class stats_callback(Callback):
	def on_epoch_end(self, epoch, logs=None):
		if (epoch + 1) % 5 == 0:
			y_pred = self.model.predict(self.test_X)

			same_bin = 0
			one_bin = 0
			two_bins = 0
			three_bins = 0

			for y_acc, y_pred in zip(self.test_Y, y_pred):
				y_acc_idx = np.argmax(y_acc)
				y_pred_idx = np.argmax(y_pred)

				print(f'\t\tacc: {y_acc_idx}\t pred: {y_pred_idx}')

				if abs(y_acc_idx - y_pred_idx) == 0:
					same_bin +=1
				if abs(y_acc_idx - y_pred_idx) <= 1:
					one_bin += 1
				if abs(y_acc_idx - y_pred_idx) <= 2:
					two_bins += 1
				if abs(y_acc_idx - y_pred_idx) <= 3:
					three_bins += 1
			
			print(f'\t\tzero_bin acc:\t{((same_bin / len(self.test_Y)) * 100):.2f} %')
			print(f'\t\tone_bin acc:\t{((one_bin / len(self.test_Y)) * 100):.2f} %')
			print(f'\t\ttwo_bin acc:\t{((two_bins / len(self.test_Y)) * 100):.2f} %')
			print(f'\t\tthr_bin acc:\t{((three_bins / len(self.test_Y)) * 100):.2f} %')

	
	def __init__(self, test_X, test_Y):
		self.test_X = test_X
		self.test_Y = test_Y

# common settings for both ptx2vec and ptx_ab approaches
# for the achieved occupancy task
@dataclass
class Aocc_settings:
	approach: str
	base_folder: str
	in_folder: str
	gen_files_folder: str
	out_folder: str
	tb_log_dir: str # tensorboard folder
	class_count: int 
	epochs: int
	batch_size: int
	dense_layer_size: int
	dropout_rate : float
	dense_regularization: float
	seed: int
	split: int # for testing purposes
	firstX: int # for testing purposes
	emb_folder: Optional[str] # parameter for ptx2vec
	ab_granularity: Optional[str] # parameter for ptx_ab
	lstm_size: Optional[int] # parameter for ptx_ab (for ptx2vec, the LSTM is set based on the embdding dim)
	emb_size: Optional[int] # parameter for ptx_ab


#####################################
####### COMMON INIT FUNCTIONS
#####################################
def get_df(base_folder, number_of_classes):
	# reads the profiling data into a pandas dataframe

	# first, generate the appropriate bins
	split = 1.0 / number_of_classes

	bins = [0]
	for i in range(number_of_classes - 1):
		bins.append(round(bins[i] + split, 2))
	bins.append(1)

	# and labels
	labels = range(number_of_classes)

	df = pd.read_csv(os.path.join(base_folder, 'prof_results.csv'))
	df['Avg_bin'] = pd.cut(df['Avg'], bins=bins, labels=labels)
	return df

def get_X_Y(df, kernels_enc, number_of_classes):
	# create training dataset out of the encoded kernels
	# and information in the loaded dataframe
	X = []
	Y = []
	Y_one_hot = []

	for _, row in df.iterrows():
		kernel_id = row['Kernel']

		# this check is only for testing with smaller datasets
		if not kernel_id in kernels_enc:
			continue

		X.append(kernels_enc[kernel_id])
		Y.append(row['Avg_bin'])
		Y_one_hot.append(task_utils.idx_to_one_hot(number_of_classes, row['Avg_bin']))

	return np.asarray(X), np.asarray(Y), np.asarray(Y_one_hot)


#####################################
####### ptx2vec
#####################################

# model used for ptx2vec approach, contrary to the devmap task, we
# have only one model
class Model_ptx2vec:
	name = 'ptx2vec'

	def init(self, seed, maxlen, emb_dim, dense_layer_size, number_of_classes,
			dropout_rate, dense_regularization):
		np.random.seed(seed)

		empty = [0.] * emb_dim

		input_layer = Input(shape=(maxlen, emb_dim), dtype='float32', name='input_layer')
		masking_layer = Masking(mask_value=empty)(input_layer)
		x = LSTM(emb_dim, implementation=1, return_sequences=True, name='lstm_1')(masking_layer)
		x = LSTM(emb_dim, implementation=1, name='lstm_2')(x)
		
		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu', kernel_regularizer=regularizers.l2(dense_regularization))(x)
		x = Dropout(rate=dropout_rate)(x)
		out = Dense(number_of_classes, activation='softmax', name='actual_out')(x)

		self.model = Model(inputs=input_layer, outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])


	def train(self, X, Y, test_X, test_Y, epochs, batch_size, log_dir):
		callbacks = []

		if log_dir is not None:
			callbacks.append(TrainValTensorBoard(log_dir, write_graph=False, profile_batch=0))
		callbacks.append(stats_callback(test_X, test_Y))

		self.model.fit(x=X, y=Y, epochs=epochs, shuffle=True, batch_size=batch_size,
			callbacks=callbacks, validation_data=(test_X, test_Y))

	def predict(self, X):
		return np.array(self.model.predict(X))

def ptx2vec_eval(settings: Aocc_settings):
	# function that initializes one of the ptx2vec models,
	# prepares kernels for training and then invokes the eval_common
	# function that actually performs the training and evaluation

	# with ptx2vec approach, we first need to load the embedding matrix
	# and vocabulary
	emb_matrix, vocab_idx, emb_stats = task_utils.ptx2vec_load_emb_files(settings.emb_folder)

	# now, preprocess and abstract all of the files that will be used
	# for training/evaluation
	preprocesser.preprocess(settings.in_folder, settings.gen_files_folder, False)	
	xfg_abstracter.abstract_ptxs(settings.gen_files_folder, vocab_idx['ab_granularity'])

	# now, load the abstracted files and encode them
	kernels_emb, maxlen = task_utils.ptx2vec_kernels_to_emb(settings.gen_files_folder, emb_matrix, vocab_idx)
	# pad the kernels to same length
	padded_kernels_emb = task_utils.ptx2vec_kernels_to_padded_emb(kernels_emb, maxlen, emb_stats['emb_dim'])

	# create model
	model = Model_ptx2vec()

	return eval_common(settings, model, padded_kernels_emb, maxlen, emb_stats['emb_dim'])

#####################################
####### ptx_ab
#####################################
# model used for ptx_ab approach, contrary to the devmap task, we
# have only one model
class Model_ptx_ab:
	name = 'ptx_ab'

	def init(self, seed, maxlen, vocab_size, dense_layer_size, number_of_classes,
			dropout_rate, dense_regularization):
		np.random.seed(seed)

		input_layer = Input(shape=(maxlen,), name='input_layer')
		emb = Embedding(input_dim=vocab_size + 1, input_length=maxlen, output_dim=self.emb_layer_size, mask_zero=True)(input_layer)
		x = LSTM(self.lstm_size, implementation=1, return_sequences=True, name='lstm_1')(emb)
		x = LSTM(self.lstm_size, implementation=1, name='lstm_2')(x)

		x = BatchNormalization()(x)
		x = Dense(dense_layer_size, activation='relu', kernel_regularizer=regularizers.l2(dense_regularization))(x)
		x = Dropout(rate=dropout_rate)(x)
		out = Dense(number_of_classes, activation='softmax', name='actual_out')(x)

		self.model = Model(inputs=input_layer, outputs=out)
		self.model.compile(optimizer='adam', metrics=['accuracy'], loss=['categorical_crossentropy'])

	def train(self, X, Y, test_X, test_Y, epochs, batch_size, log_dir):
		callbacks = []
		if log_dir is not None:
			callbacks.append(TrainValTensorBoard(log_dir, write_graph=False, profile_batch=0))
		callbacks.append(stats_callback(test_X, test_Y))

		self.model.fit(x=X, y=Y, epochs=epochs, shuffle=True, batch_size=batch_size,
			callbacks=callbacks, validation_data=(test_X, test_Y))

	def predict(self, X):
		return np.array(self.model.predict(X))

	def __init__(self, lstm_size, emb_layer_size):
		self.lstm_size = lstm_size
		self.emb_layer_size = emb_layer_size


def ptx_ab_eval(settings: Aocc_settings):
	# function that initializes the ptx_ab models,
	# prepares kernels for training and then invokes the eval_common
	# function that actually performs the training and evaluation
	ab_granularity = settings.ab_granularity

	# perform the preprocessing and abstraction of the task input files
	preprocesser.preprocess(settings.in_folder, settings.gen_files_folder, False)
	xfg_abstracter.abstract_ptxs(settings.gen_files_folder, ab_granularity)

	# now, contrary to the ptx2vec approach, we need to create the vocabulary
	# as there is no vocabulary that was produced beforehand for this approach
	vocab_idx = task_utils.ptx_ab_create_vocabulary(settings.gen_files_folder)

	# encode the kernels of the task input files
	kernels_ind, maxlen = task_utils.ptx_ab_emb_kernels_to_indices(settings.gen_files_folder, vocab_idx)
	# pad the kernels to the same length
	kernels_ind_padded = task_utils.ptx_ab_emb_kernels_padded(kernels_ind, maxlen)

	# create model
	model = Model_ptx_ab(settings.lstm_size, settings.emb_size)

	# and perform the training and evaluation
	return vocab_idx, eval_common(settings, model, kernels_ind_padded, maxlen, len(vocab_idx))

#####################################
####### TRAINING AND EVAL
#####################################
def eval_common(settings: Aocc_settings, model, padded_kernels_enc: dict, maxlen, model_shape_param):
	results = []
	
	# get the training data
	df = get_df(settings.base_folder, settings.class_count)
	X, Y, Y_one_hot = get_X_Y(df, padded_kernels_enc, settings.class_count)

	# use stratified n-fold cross validation 
	kf = StratifiedKFold(n_splits=settings.split, shuffle=True, random_state=settings.seed)
	for i, (train_indices, test_indices) in enumerate(kf.split(X, Y)):
		print(f'**** model {model.name}, split {i} (out of {settings.split}) ****')

		# train data for this split
		X_train = X[train_indices]
		Y_train = Y_one_hot[train_indices]
		
		# test data for this split
		X_test = X[test_indices]
		Y_test = Y_one_hot[test_indices]

		# initialize the model
		model.init(settings.seed, maxlen, model_shape_param, settings.dense_layer_size,
			 settings.class_count, settings.dropout_rate, settings.dense_regularization)

		# perform the training
		model.train(X_train, Y_train, X_test, Y_test, settings.epochs, settings.batch_size, settings.tb_log_dir)

		# create predictions of the testing fold
		Y_pred = model.predict(X_test)

		# and append the results to the final dataframe
		y_acc_bins = []
		y_pred_bins = []
		for y_acc, y_pred in zip(Y_test, Y_pred):
			y_acc_bins.append(np.argmax(y_acc))
			y_pred_bins.append(np.argmax(y_pred))

		for y_acc_bin, y_pred_bin in zip(y_acc_bins, y_pred_bins):
			results.append({
				'approach' : settings.approach,
				'y_actual_bin' : y_acc_bin,
				'y_pred_bin' : y_pred_bin
			})

	return pd.DataFrame(results, columns = [
		'approach',
		'y_actual_bin',
		'y_pred_bin'
	])

def main(argv):
	# for arguments, we are using the absl library, the original flags are ommited
	del argv

	# folders
	aocc_folder = FLAGS.aocc
	in_folder = FLAGS.aocc_in
	gen_files_folder = FLAGS.aocc_gen_files
	out_folder = FLAGS.aocc_out

	# in the case of tensor baord logging
	tb_log_dir = None
	if FLAGS.aocc_tb_logging:
		tb_folder = os.path.join(aocc_folder, FLAGS.aocc_tb_folder)
		tb_model_name = FLAGS.aocc_tb_model_name if FLAGS.aocc_tb_model_name is not None else datetime.now().strftime("%Y%m%d-%H%M%S")
		tb_log_dir = os.path.join(tb_folder, tb_model_name)

	# which approach - ptx2vec or ptx_ab
	approach = FLAGS.aocc_approach

	# training settings
	class_count = FLAGS.aocc_class_count
	epochs = FLAGS.aocc_epochs
	batch_size = FLAGS.aocc_batch_size
	dense_layer_size = FLAGS.aocc_dense_layer_size
	dropout_rate = FLAGS.aocc_dropout_rate
	dense_regularization = FLAGS.aocc_dense_regularization
	seed = FLAGS.aocc_seed if FLAGS.aocc_seed != 0 else np.random.randint(1000)
	split = FLAGS.aocc_split
	firstX = FLAGS.aocc_firstX

	# create the settings
	settings = Aocc_settings(
		approach,
		aocc_folder,
		in_folder,
		gen_files_folder,
		out_folder,
		tb_log_dir,
		class_count,
		epochs,
		batch_size,
		dense_layer_size,
		dropout_rate,
		dense_regularization,
		seed,
		split,
		firstX,
		None,
		None,
		None,
		None
	)

	# perform the training and get the results in a dataframe
	if approach == 'ptx2vec':
		settings.emb_folder = FLAGS.trained_emb
		df = ptx2vec_eval(settings)
	elif approach == 'ptx_ab':
		settings.ab_granularity = FLAGS.aocc_ab_granularity
		settings.lstm_size = FLAGS.aocc_lstm_size
		settings.emb_size = FLAGS.aocc_emb_size
		vocab_idx, df = ptx_ab_eval(settings)

	# writing down everything to files ...
	# detect the number of folders in the out folder (the resulting
	# folder will then be "res_<len>")
	res_num = len([i for i in os.listdir(os.path.join(out_folder, '')) if os.path.isdir(os.path.join(out_folder, i))])
	res_folder_name = f'res_{res_num}'
	res_folder_path = os.path.join(out_folder, res_folder_name)
	os.mkdir(res_folder_path)

	# if we use ptx2vec approach, copy some of the related files to the
	# output folder
	if approach == 'ptx2vec':
		task_utils.copy_ptx2vec_related_files(res_folder_path, settings.emb_folder)

	# outputing the dataframe with results
	df.to_csv(os.path.join(res_folder_path, 'results.csv'), sep='\t', index=False)

	# also, report the overall settings
	exp_settings = {
		'approach' : approach,
		'class_count': class_count,
		'epochs': epochs,
		'batch_size': batch_size,
		'dense_layer_size': dense_layer_size,
		'dropout_rate' : dropout_rate,
		'dense_regularization': dense_regularization,
		'seed': seed
	}

	# if we use ptx_ab approach, output some additional settings
	if approach == 'ptx_ab':
		exp_settings['ab_granularity'] = settings.ab_granularity
		exp_settings['lstm_size'] = settings.lstm_size
		exp_settings['emb_size'] = settings.emb_size
		exp_settings['vocab_idx'] = vocab_idx

	settings_path = os.path.join(res_folder_path, 'settings.json')
	utils.write_json(settings_path, exp_settings)


if __name__ == '__main__':
	app.run(main)



