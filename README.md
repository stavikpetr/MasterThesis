# Master thesis - analysing and optimizing GPU kernels with machine learning
This repository contains the evaluation and code of our two developed approaches that we call **ptx2vec** and **ptx_ab**. Both of these approaches are based on Recurrent neural networks (RNNs) over PTX code, the intermediate code for CUDA. These approaches are evaluated on two different tasks -- heterogeneous device mapping task and achieved occupancy task.

**ptx2vec** approach is inspired by the work of Ben-Nun et al. (https://arxiv.org/abs/1806.07336) that used RNNs over LLVM IR code. In this work, Ben-Nun et al. defined contextual flow graphs (XFGs) that capture data and control dependencies between instructions. These dependencies can be in turn used to obtain contextual pairs of instructions, which can then be used to train the Skip-Gram model that learns to map instructions with similar contexts to similar embedding vectors. These embedding vectors can then be used for any GPU optimization task.

**ptx_ab** approach is much simpler. In this approach, there are no pre-trained embeddings. Instead, the embedding space is trained in tandem with each GPU optimization task. In addition, contrary to ptx2vec, this approach works with a much coarser granularity of instructions.

You can find the **text** of the thesis in the *text* folder.

## Requirements and Installation
The code in this repository requires only python and a few additional packages. You will need python >= 3.7 (python 3.6 should also work, but you will need to perform one additional step with this version) and packages that are listed in file *requirements.txt*. For installing the packages, we strongly suggest using virtual environments (https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/). After you activated your python virtual environment, you can run `pip install -r requirements.txt` which will install the required packages. If you are using python 3.6, you need to install one additional package that became part of python 3.7. This is package dataclasses that can be installed with `pip install dataclasses`.

With this setup, you can run all of the python code in this repository. However, the one bash script in this repository, that is used for building the ptx2vec dataset, requires CUDA compiler for compiling CUDA files into PTX. Therefore, if you also want to run this script (this is not necessary, because we included a download link for the whole dataset -- this link is mentioned in the Quick start section), you will need CUDA SDK which includes the compiler. We used compiler that is included in CUDA SDK 10.2, but any version above 10 should be fine. You can download this SDK for both Windows and Linux from https://developer.nvidia.com/cuda-downloads. In addition, you will need a version of g++ (or other native c++ compiler) that supports c++11.

## Quick start
All of our results that were presented in the thesis can be found in this repository. The ptx2vec embedding space that we used in both optimization tasks can be found in *ptx2vec_out/ptx2vec_pretrained*. The results for heterogeneous device mapping task can be found in *task_devmap/out* and the results for achieved occupancy task can be found in *task_aocc/out*. If you want to perform the training and evaluation yourself, then:

1. Training of ptx2vec embedding space 
    - go to the directory *ptx2vec_in* and run the available bash script *gather_ptx_from_pga.sh*; this script goes over the public GitHub repositories that are listed in *ptx2vec_in/cuda_repos_all* (that were gathered with the use of Public Git Archive (https://github.com/src-d/datasets/tree/master/PublicGitArchive)), it tries to clone each repository and compile the CUDA files that are present in it (the whole process takes about 4 hours); alternatively, you can download the whole dataset that we used from  https://drive.google.com/file/d/17W9zmG_o-znCJMG5L3KmllKSy4ipnLv9/view?usp=sharing and extract it in *ptx2vec_in* folder
    - run `python train_ptx2vec.py --ptx2vec_mode=gen_train_data`, this generates the training data for the Skip-Gram model (again, takes a long time, about 6 hours for the whole dataset), or alternatively, if you only want to test the functionality, you can delete the majority of folders in folder *ptx2vec_in* and the training data will be generated only based on the contents of *ptx2vec_in* folder
    - run `python train_ptx2vec.py --ptx2vec_mode=train_skipgram` which performs the actual training; the training is controlled by flags that can be seen in *src_common/appflags.py*
    - after the training, you can find the resulting embedding space (saved as an embedding matrix) along with the vocabulary in *ptx2vec_out*
2. Training and evaluation of heterogeneous device mapping task
    - observe the flags in *src_common/appflags.py*, the most important flags are `--devmap_benchmark`, `--devmap_approach`, `--devmap_imm`
    - for example, if you want to train and evaluate ptx2vec approach with immediate values over the AMD benchmark, then you can run `python train_task_devmap.py --devmap_benchmark=amd --devmap_approach=ptx2vec --devmap_imm=True` (by default, our pre-trained embeddings that can be found in *ptx2vec_out/ptx2vec_pretrained* are used)
    - after the training, the results can be found in *task_devmap/out/*
3. Training and evaluation of achieved occupancy task
    - observe the flags in *src_common/appflags.py*, the most important flag is `--aocc_approach`
    - for example, if you want to train and evaluate ptx_ab approach then you can run `python train_task_aocc.py --aocc_approach=ptx_ab` (by default, our pre-trained embeddings that can be found in *ptx2vec_out/ptx2vec_pretrained* are used)
    - after the training, the results can be found in *task_aocc/out/*

## Repository overview
This repository can be logically divided into four important parts:
- First, there are files that are related to the training of ptx2vec embedding space with the use of XFGs and the Skip-Gram model.
- The second part consists of files that are related to the implementation of ptx_ab approach (note however that there are no separate files for this approach, because its simplicity allowed us to embed it into the remaining files).
- The third part is composed of files that are related to the heterogeneous device mapping task.
- Lastly, there are files that are related to the achieved occupancy task.

These parts are mapped to the files and folders in this repository in the following way:
- *ptx2vec_in* - folder with input data for training of ptx2vec embedding space
- *ptx2vec_gen_files* - folder for miscellaneous files that are generated during the training of ptx2vec embedding space
- *ptx2vec_out* - folder that will contain the resulting ptx2vec embedding space after training
- *src_common* - folder with application flags and other utility files that are used throughout the project
- *src_ptx2vec* - folder with the majority of code that implements ptx2vec approach (some of these files also implement the core functionality of ptx_ab approach)
- *task_aocc* - folder for the achieved occupancy task
    - *task_aocc/in* - folder with input kernels in PTX
    - *task_aocc/gen_files* - folder for various files that are generated during the training and evaluation
    - *task_aocc/out* - folder that will contain the results for this task
    - *task_aocc/prof_results.csv* - profiling results that together with input kernels form the whole training dataset
- *task_devmap* - folder for the heterogeneous device mapping task
    - *task_devmap/in* - folder with input kernels in PTX
    - *task_devmap/gen_files* - folder for various files that are generated during the training and evaluation
    - *task_devmap/out* - folder that will contain the results for this task
    - *task_devmap/cgo17-amd.csv* - measurements for AMD platform that together with input kernels form the whole training dataset
    - *task_devmap/cgo-nvidia.csv* - measurements for NVIDIA platform 
- *train_ptx2vec.py* - performs the training of ptx2vec embedding space
- *train_task_aocc.py* - performs the training and evaluation of the achieved occupancy task
- *train_task_devmap.py* - performs the training and evaluation of the heterogeneous device mapping task

## Implementation overview
As already noted, there are three top-level files that can be executed, namely *train_ptx2vec.py*, *train_task_aocc.py*, *train_task_devmap.py* and that perform the training of ptx2vec embedding space, heterogeneous device mapping task and achieved occupancy task respectively. Apart from these three files, there are two more folders with code. Folder *src_common* contains file with application flags (*src_common/appflags.py*) and two files with utility functions (*src_common/utils.py* and *src_common/task_utils.py*). Lastly, folder *src_ptx2vec* contains code that implements the most important parts of the training of ptx2vec embedding space.

Because the implementation of ptx2vec is quite complicated, we decided to describe it in a separate README file that can be found under *src_ptx2vec*. The remaining three parts are overall much simpler and we can briefly describe them here.

#### Implementation of ptx_ab
The implementation of ptx_ab approach is very simple. As already noted, there are no intermediate results produced by this approach and the vocabulary is generated separately for each task. This means that the implementation of this approach revolves around functionality that transforms each instruction into its abstract form. Because there is already a similar functionality implemented, in the *src_ptx2vec/xfg_abstracter.py*, we decided to put the abstraction function of ptx_ab there. The remaining functionality of this approach, which is building of vocabulary is embedded in each of the task files.

#### Implementation of tasks
The training and evaluation of models for both heterogeneous device mapping and achieved occupancy tasks is implemented in a single file. File *train_task_devmap.py* performs the heterogeneous device mapping (devmap) task, while file *train_task_aocc.py* performs the achieved occupancy task (aocc). In addition, there is the file *src_common/task_utils.py* that contains some task utility functions.

The general implementation of both of these tasks is very similar. First, the input kernels are preprocessed. This step uses the same preprocesser of PTX files that is implemented in file *src_ptx2vec/preprocesser.py*. Then, the instructions of these files are transformed into an abstract version. Again, this step uses the abstraction functionality that is implemented in *src_ptxvec/xfg_abstracter.py*. In the case of ptx_ab approach, this step is followed by the building of vocabulary. Then, the input kernels are encoded. In the case of ptx_ab, they are encoded in one-hot representation, in the case of ptx2vec, they are mapped to the embedding vectors. Then, we load the measurement data, combine them with the encoded kernels and we have actual training data available. Lastly, we initialize class that represents the machine learning model and we perform the training. After the training, the results are stored in the respective output folders.