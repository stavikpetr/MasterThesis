Base folder for achieved occupancy task.

Structure:

- in - folder with input kernels
- gen_files - folder that will contain various files that were generated during the training and evaluation of this task
- out - folder that will contain the training results
- prof_results.csv - profiling results with measurements of achieved occupancy