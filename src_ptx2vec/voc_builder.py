# This file implements vocabulary building phase of the ptx2vec approach.
#
# This phase builds a vocabulary out of all abstract instructions that we obtained
# in the XFG abstraction phase. Vocabulary is implemented as a dictionary where
# each key is an instruction and its value is a unique vocabulary index (this index can
# be later used for the encoding of this instruction).
#
# This phase has one important parameter -- cutoff. All instructions that appear
# less than cutoff times are discarded from the vocabulary
#
# Note that the vocabulary building step of ptx_ab approach is not implemented in this
# file. The reason is, that in that particular case, the vocabulary building step
# is extremely simple. For that reason, the vocabulary building step of ptx_ab
# approach is implemented in the tasks and not here.

import os

from src_common import utils

def add_file_to_vocabs(vocab_freq, vocab_idx, ab_file_content):
	# this function simply adds each instruction of the file
	# into the vocabularies
	ab_file_lines = ab_file_content.splitlines()

	for l in ab_file_lines:
		if not l in vocab_idx:
			vocab_idx[l] = len(vocab_idx)

		if l in vocab_freq:
			vocab_freq[l] += 1
		else:
			vocab_freq[l] = 1


def vocab_cutoff_split(vocab_idx, vocab_freq, cutoff):
	# this function applies cutoff to the vocab_idx vocabulary
	new_vocab_idx = {}
	vocab_cutoff = {}

	for stmt, count in vocab_freq.items():
		if count >= cutoff:
			new_vocab_idx[stmt] = len(new_vocab_idx)
		else:
			vocab_cutoff[stmt] = len(vocab_cutoff)

	return new_vocab_idx, vocab_cutoff

#####################################
####### PUBLIC API
#####################################
def build_vocabulary(generated_files_folder, restore, cutoff, ab_granularity):
	# this is the main function of the vocabulary building phase;
	#
	# it goes over each ab_stmnts.txt file that was produced in the XFG abstraction
	# phase (these files essentially contain all of the abstract instructions of the
	# XFGs) and from these files, it builds the vocabulary with abstract instructions
	#
	# this function outputs three files: vocab_freq contains all of the abstract
	# instructions along with their frequency; vocab_idx is the actual vocabulary
	# that can be used for encoding of instructions; vocab_coff is the vocabulary
	# with statements that were not placed to vocab_idx because of cutoff
	# or in other words:
	# frequency vocabulary contains all statements - regardless of 
	# cutoff ... vocab_idx contains only statements that were not cutoff
	# and vocab_coff contains only cutoff statements

	utils.report_phase_started('building vocabulary')

	vocab_freq_n = 'vocab_freq.json'
	vocab_idx_n = 'vocab_idx.json'
	vocab_coff_n = 'vocab_coff.json'
	vocab_freq_fp = os.path.join(generated_files_folder, vocab_freq_n)
	vocab_idx_fp = os.path.join(generated_files_folder, vocab_idx_n)
	vocab_coff_fp = os.path.join(generated_files_folder, vocab_coff_n)

	if restore and os.path.exists(vocab_idx_fp):
		return

	vocab_freq = {}
	vocab_idx = {}

	# first, add each file to vocab_freq and vocab_idx
	for _, _, ab_stmnts in utils.read_files(generated_files_folder,
		'_ab_stmnts.txt', utils.read_text_f):

		add_file_to_vocabs(vocab_freq, vocab_idx, ab_stmnts)

	# then, apply cutoff
	if cutoff > 0:
		vocab_idx, vocab_coff = vocab_cutoff_split(vocab_idx, vocab_freq, cutoff)
		utils.write_json(vocab_coff_fp, vocab_coff, indent=4, sort_keys=False)

	unk_stmt_idx = len(vocab_idx)
	vocab_idx['UNK_STMT'] = unk_stmt_idx

	vocab_idx = {'cutoff' : cutoff, 'unk_stmt_idx' : unk_stmt_idx,
		'ab_granularity' : ab_granularity, 'vocab_idx' : vocab_idx}
	
	# and output the vocabularies
	utils.write_json(vocab_freq_fp, vocab_freq, indent=4, sort_keys=True)
	utils.write_json(vocab_idx_fp, vocab_idx, indent=4, sort_keys=False)