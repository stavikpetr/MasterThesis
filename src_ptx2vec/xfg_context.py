# This file implements functionality that is related to 
# collection of context of a single PTX file.
# 
# There are two types of context that can be collected in XFG ...
# we can either collect global context that contains all
# global variables and global functions or we can collect context
# for a single function. Function context contains parameters,
# other local variables, labels, registers and so on.
#
# The contexts are used in two modules, or phases -- XFG building phase
# and XFG abstraction phase. In XFG building phase, it allows
# classify the operands of a single instruction -- if we have
# instruction xyz.u32 var1, var2, var3; then we need to know
# what the operands var1, var2 and var3 represent ... are they
# registers or paramatars or other global variables? Context
# allows to detect this easily, which allows to correctly
# create the nodes and connections in the XFG.
#
# In the XFG abstraction phase, the context is used in much
# the same way but only with a different reason. In XFG abstraction
# phase, each instruction needs to be converted into its abstract
# form like xyz.u32 v1, v2, v3; -> xyz.u32 <REG>, <REG>, <L_VAR>;
# and to do that, we need the context

import re
from dataclasses import dataclass 

# ptx2vec imports
from src_common import utils
from src_ptx2vec import ptx_info as ptx

@dataclass
class global_context:
	global_vars : list
	function_names : list
	ext_function_names : list

@dataclass
class function_context:
	f_name : str
	regs : list
	labels : list
	params : list
	other_vars : list
	branch_targets : dict
	last_line : str = ''


def collect_g_context(ptx_file_lines) -> global_context:
	g_context = global_context([], [], [])
	idx = 0 

	def get_glob_var_name(line):
		line_split = line.split('=')
		return utils.find_all_extract_last(ptx.r_variable_identifier, line_split[0])

	def get_func_name(line):
		return utils.find_all_extract_last(ptx.r_func_identifier, line)

	while idx < len(ptx_file_lines):
		line = ptx_file_lines[idx]
		
		# the order here is important
		if re.match(ptx.r_oneline_declared_func, line):
			g_context.ext_function_names.append(get_func_name(line))
			idx += 1
		elif re.match(ptx.r_global_variable, line):
			g_context.global_vars.append(get_glob_var_name(line))
			idx += 1
		elif re.match(ptx.r_oneline_defined_func, line) or re.match(ptx.r_oneline_kernel, line):
			if ptx_file_lines[idx+1] != '{':
				utils.error("collecting g context - expecting bracket after function definition")
			g_context.function_names.append(get_func_name(line))
			enclosing_idx = utils.find_enclosing_bracket(ptx_file_lines, idx + 1)
			idx = enclosing_idx + 1
		else:
			idx += 1
	
	return g_context

def collect_f_context(f_def_line, f_body_lines) -> function_context:
	f_context = function_context('', [], [], [], [], {})

	# function name
	f_context.f_name =  utils.find_all_extract_last(ptx.r_func_identifier, f_def_line)

	# parameters
	# contains both return values and actual parameters
	f_params_list = re.findall(ptx.r_func_params, f_def_line)

	for f_params in f_params_list:
		f_params_split = f_params.split(',')

		for f_param in f_params_split:
			# function with no arguments
			if not 'param' in f_param:
				continue
			param_identifier = utils.find_all_extract_last(ptx.r_variable_identifier, f_param)
			f_context.params.append(param_identifier)

	for line in f_body_lines:
		# registers, other variables 
		if re.match(ptx.r_local_variable, line):
			var_decl_split = line.split(',')
			var_decl_split[0] = utils.find_all_extract_last(ptx.r_variable_identifier, var_decl_split[0])
	
			registers = False
			if re.match('\.reg', line):
				registers = True	
	
			# add the rest of the identifiers
			for i in range(len(var_decl_split)):
				var_i_id = re.search(ptx.r_variable_identifier, var_decl_split[i]).group(0)
				if registers:
					f_context.regs.append(var_i_id)
				else:
					f_context.other_vars.append(var_i_id)
		# labels
		elif re.match(ptx.r_label, line):
			label = re.search(ptx.r_label, line).group(1)
			f_context.labels.append(label)

			if 'targets' in line:
				targets = [] 
				line_split = line.split(',')
				targets.append(utils.find_all_extract_last(ptx.r_variable_identifier, line_split[0]))
				for i in range(1, len(line_split)):
					targets.append(re.search(ptx.r_variable_identifier, line_split[i]).group(0))
				if '.branchtargets' in line:
					f_context.branch_targets[label] = targets
	
	return f_context
