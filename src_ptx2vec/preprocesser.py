# This file contains preprocesser of PTX that is used in various contexts
# throughout the project.
#
# 1) it is primarily used in ptx2vec as it implements the first phase --
# -- preprocessing phase that transforms each PTX file into a normalized form
# before it is passed into the next phase, XFG building phase
# 2) the second use of preprocesser is in each task -- before we can train
# any model over PTX files, we need to perform the preprocessing (the other 
# two steps are abstraction and encoding)

import os
import re

from src_common import utils
from src_ptx2vec import ptx_info as ptx

# global variable for mangling of scoped identifiers
file_mangle_counter = 0

def remove_module_directives(ptx_file_content):
	# each PTX starts with a few module directives that 
	# we do not care about 

	def version_check_and_sub(version_match):
		version_str = version_match.group(0)
		version = version_str.split(' ')[1]

		if version != ptx.PTX_SUPPORTED_VERSION:
			utils.warning('ptx versions differ - this means that some of the instructions may not be supported')

		# return empty string for substition
		return ''
	
	def target_check_and_sub(target_match):
		target_str = target_match.group(0)
		target = target_str.split(' ')[1]

		if(target != ptx.SM_SUPPORTED_TARGET):
			utils.warning('sm versions differ - this may result in a less complex instructions')

		# return empty string for substitution
		return ''

	ptx_file_content = re.sub(ptx.r_version, version_check_and_sub, ptx_file_content)

	ptx_file_content = re.sub(ptx.r_target, target_check_and_sub, ptx_file_content)

	ptx_file_content = re.sub(ptx.r_address_size, '', ptx_file_content)

	return ptx_file_content

def remove_perf_tuning_directives(ptx_file_content):
	# all of the following directives can be placed in the PTX
	# for the subsequent compilation stages - thus, we dont care about them

	# remove maxnreg directive
	ptx_file_content = re.sub(ptx.r_maxnreg, '', ptx_file_content)

	# remove maxntid directive
	ptx_file_content = re.sub(ptx.r_maxntid, '', ptx_file_content)

	# remove reqntid directive
	ptx_file_content = re.sub(ptx.r_reqntid, '', ptx_file_content)

	# remove minnctaspersm directive
	ptx_file_content = re.sub(ptx.r_minnctaspersm, '', ptx_file_content)

	# remove maxnctaspersm directive
	ptx_file_content = re.sub(ptx.r_maxnctaspersm, '', ptx_file_content)

	# remove noreturn directive
	ptx_file_content = re.sub(ptx.r_noreturn, '', ptx_file_content)
	
	# remove pragma directives
	ptx_file_content = re.sub(ptx.r_pragma, '', ptx_file_content)

	return ptx_file_content

def remove_linking_directives(ptx_file_content):
	# apart from the .extern directive, we may remove all of the
	# other linking directives

	# remove visible linking directive
	ptx_file_content = re.sub(ptx.r_visible, '', ptx_file_content)

	# remove weak linking directive
	ptx_file_content = re.sub(ptx.r_weak,
		 lambda match: re.sub('\.weak', '', match.group(0)),
		 ptx_file_content)

	# remove common linking directive
	ptx_file_content = re.sub(ptx.r_common, '', ptx_file_content)

	return ptx_file_content

def remove_unnecessary_directives(ptx_file_content):
	# in this function, we remove some of the obviously useless directives
	# that will interfer with the next phases where we process the file
	# line by line
	ptx_file_content = remove_module_directives(ptx_file_content)
	ptx_file_content = remove_perf_tuning_directives(ptx_file_content)
	ptx_file_content = remove_linking_directives(ptx_file_content)

	return ptx_file_content

def remove_comments(ptx_file_content):
	# simple substition of all comments in PTX (C-like comments)
	return re.sub(ptx.r_comments, '', ptx_file_content, flags=re.S)

def collapse_statements(ptx_file_content):
	# in the subsequent phases (xfg building), we will process the file line by line
	# therefore, it is convenient to collapse some statements

	def collapse_match(match):
		match_string = match.group()
		match_string = re.sub('\n', '', match_string)
		return re.sub('\s+', ' ', match_string) # cosmetic substition

	# collapse kernel definitions
	ptx_file_content = re.sub(ptx.r_collapse_kernel, collapse_match,
		 ptx_file_content, flags=re.S)

	# collapse call statements
	ptx_file_content = re.sub(ptx.r_collapse_call, collapse_match,
		ptx_file_content, flags=re.S)

	def collapse_functions(match):
		# we have two groups in the passed match - if the second group is ';' we have 
		# either fwd declared or externally declared function and we want to first
		# join the strings and then remove all the newlines and return that
		# ... in case it is not ';', then we simply take the first group
		# and remove all the newlines and then concat the second match group
		match_str_1 = match.group(1)
		match_str_2 = match.group(2)
		match_str = ''

		if match_str_2 == ';':
			match_str = match_str_1 + match_str_2
		else:
			match_str = match_str_1

		match_str = re.sub('\n', '', match_str)
		match_str = re.sub('\s+', ' ', match_str) # cosmetic substition

		if match_str_2 != ';':
			match_str = match_str + match_str_2

		return match_str
		
	# collapse functions (both defined, declared (external) and fwd declared)
	ptx_file_content = re.sub(ptx.r_collapse_func, collapse_functions,
		ptx_file_content, flags=re.S)

	return ptx_file_content

def remove_fwd_declared_functions(ptx_file_content):
	# forward declared functions (not external ones) are useless to
	# us and we can delete them
	# note that by now, all of the function declarations have been
	# collapsed which makes the regex simpler

	def remove_internal_fwd_declared_function(match_group):
		match_str = match_group.group(0)
		if '.extern' in match_str:
			return match_str
		return ''

	ptx_file_content = re.sub(ptx.r_oneline_fwd_declared_func, 
		remove_internal_fwd_declared_function, ptx_file_content, flags=re.M)
	
	return ptx_file_content

def remove_var_initialization(ptx_file_content):
	# we can also replace all variable intiializers with {...}
	# this is done mainly for improved readability of PTX files
	def sub(match):
		m = match.group(0)
		return re.sub('\{.*\}', '{ ... }', m)

	ptx_file_content = re.sub(ptx.r_variable_init, sub, ptx_file_content)

	return ptx_file_content

def uncollapse_function_scopes(ptx_file_content):
	# we also need to uncollapse some lines; this uncollapsing
	# may be necessary due to examples like
	# {  .reg .u32 r0;  .reg .pred p; }
	# that were placed in the ptx file by e.g. inline asm
	# currently, this function works for a sensible subset of 
	# cases ... however, it does not handle all of the weird cases that
	# can come up, like multiple nested empty blocks and so on

	ptx_file_lines = ptx_file_content.splitlines()
	
	def uncollapse_match(match):
		# first group is the semicolon,
		# second group is everything to the next semicolon (excluding it)
		return match.group(1) + '\n' + match.group(2)

	# first, we place each of the .reg from the previous example
	# on their own lines
	ptx_file_content = re.sub(ptx.r_uncollapse_2stmnts, uncollapse_match, ptx_file_content)

	# now first case of trailing bracket
	# .reg .u32 r0; { ... (new scope)
	# => .reg .u32 r0; \n{
	ptx_file_content = re.sub(ptx.r_uncollapse_t1_bracket, ';\n{', ptx_file_content)

	# now { ... } { ... } => { ... } \n { ... }
	def uncollapse_pt_bracket(match):
		return match.group(1) + '\n' + match.group(2) + '\n' + match.group(3)

	ptx_file_content = re.sub(ptx.r_uncollapse_pt_bracket, uncollapse_pt_bracket, ptx_file_content)

	# now, each scope starting bracket should be the first non
	# whitespace character at the beggining of line, which
	# means we can do the following

	def sub(match):
		return '{\n' + match.group(2)

	# now, we uncollapse statement preceeding bracket, that means
	# { .reg .u32 r0; => {\n .reg .u32 r0;
	ptx_file_content = re.sub(ptx.r_uncollapse_p_bracket, sub, ptx_file_content, flags=re.M)

	# now, uncollapse statement trailing bracket
	# .reg .pred p; } => .reg .pred p; \n}
	ptx_file_content = re.sub(ptx.r_uncollapse_t2_bracket, ';\n}', ptx_file_content)

	return ptx_file_content

def replace_parametrized_variable_declarations(ptx_file_content):
	# in this function we replace parametrized variable declarations
	# like this: .reg .b32 %r<100> => .reg .b32 %r0, %r1, %r2, ...
	# this is performed mainly for convienience of the following phases

	def substitute_match(match):
		par_var_decl = match.group(0)
		# extract the identifier and number 
		var_id = re.search(ptx.r_variable_identifier, par_var_decl).group(0)
		var_num = int(re.search(ptx.r_parametrized_variable_number, par_var_decl).group(0))

		s = ''
		for i in range(var_num):
			s+= var_id + str(i)
			if i != var_num - 1:
				s += ', '

		return s

	ptx_file_content = re.sub(ptx.r_parametrized_variable_declaration, 
		substitute_match, ptx_file_content)

	return ptx_file_content

def replace_compile_time_constants(ptx_file_content):
	# while compiling code from LLVM IR to PTX (heterogeneous device mapping task)
	# the resulting files contain some compile time constants
	ptx_file_content = re.sub('\\bWARP_SZ\\b', '32', ptx_file_content)
	return ptx_file_content

def strip_lines(ptx_file_content):
	# removing spaces at the start and end of lines
	ptx_file_lines = ptx_file_content.splitlines()
	ptx_file_lines = [line.strip() for line in ptx_file_lines]
	ptx_file_content = '\n'.join(ptx_file_lines)

	return ptx_file_content

def mangle_scoped_identifiers_single_scope(scope_lines):
	# this function is capable of mangling single function scope
	global file_mangle_counter

	for i in range(len(scope_lines)):
		line = scope_lines[i]
		if line == '{' or line == '}':
			utils.error('can not mangle scoped identifiers - found nested scope')

		if re.match(ptx.r_scoped_variable_declaration, line):
			all_declarations = line.split(',')

			# we need to be careful - declaration[0] contains other
			# parts of the statements
			all_declarations[0] = utils.find_all_extract_last(ptx.r_variable_identifier,
				all_declarations[0])

			for decl in all_declarations:
				old_var_name = re.search(ptx.r_variable_identifier, decl).group(0)
				new_var_name = old_var_name + '_' + str(file_mangle_counter)
				file_mangle_counter += 1

				for j in range(i, len(scope_lines)):
					if old_var_name[0].isalpha():
						scope_lines[j] = re.sub('\\b' + old_var_name + '\\b', new_var_name, scope_lines[j])
					else:
						scope_lines[j] = re.sub(old_var_name + '\\b', new_var_name, scope_lines[j])

	return scope_lines

def mangle_scoped_identifiers(ptx_file_content):
	# this function mangles scoped indentifiers in the whole file
	# by going over each function body and mangling the scoped identifiers
	# inside these function bodies
	# mangling is performed because ... if we have 2 scopes that e.g. call some 
	# function, nvcc generates registers and variables with indetical names
	# and that would later cause problems during xfg graph generation
	#
	# in addition, this function also removes the scope all together -- this is
	# performed because the following phase assumes that there are nested scopes
	# in the functions
	ptx_file_lines = ptx_file_content.splitlines()
	# reset mangle counter
	file_mangle_counter = 0

	for _, _, f_body_lines, start_idx in utils.collect_functions(ptx_file_lines):
		idx = 0
		while idx < len(f_body_lines):
			line = f_body_lines[idx]
			# we have a scope
			if line == '{':
				enclosing_idx = utils.find_enclosing_bracket(f_body_lines, idx)
				scope_lines = f_body_lines[idx + 1:enclosing_idx]

				mangled_scope_lines = mangle_scoped_identifiers_single_scope(scope_lines)

				# we want to actually remove the scope brackets here
				f_body_lines[idx] = ''
				f_body_lines[enclosing_idx] = ''

				for i, j in zip(range(idx + 1, enclosing_idx), range(0, len(mangled_scope_lines))):
					f_body_lines[i] = mangled_scope_lines[j]

				idx = enclosing_idx + 1
			else:
				idx += 1
		for i in range(0, len(f_body_lines)):
			ptx_file_lines[start_idx + i] = f_body_lines[i]

	ptx_file_content = '\n'.join(ptx_file_lines)

	return ptx_file_content

def preprocess_labels(ptx_file_content):
	# in this function, our task is to detect lines like:
	# L1: add %r1, %r2, %r3 and uncollapse them to two lines
	# ... this is mainly for convenience in later phases
	# however, note that there are three cases where we do not want to perform 
	# this substition and that is in cases that also use one of the
	# .branchtargets/.calltargets/.callprototype directives
	def substitute_label(match):
		label_line = match.group(0)
		if '.branchtargets' in label_line or '.calltargets' in label_line or '.callprototype' in label_line:
			return label_line
		else:
			label_line = re.sub(':', ':\n', label_line)
			return label_line

	ptx_file_content = re.sub(ptx.r_lpreprocess_label, substitute_label, ptx_file_content)

	return ptx_file_content

def add_ad_hoc_labels(ptx_file_content):
	# we want to add ad_hoc labels in a few different scenarios

	# First, we basically want to remove all
	# the lines with predicated execution, except the ones that
	# contain brx or bra instructions ... what do we mean by that..
	# setp.lt.s32  p, i, n;
	# @p add.s32   j, j, 1;
	# =====>
	# setp.lt.s32  p, i, n;
	# @!p bra L1;
	# add.s32   j, j, 1;
	# L1:
	# ...
	# this is very vital for the subsequent phases as it allows
	# to substantially reduce potential size of the vocabulary
	file_label_counter = 0
	def substitute_pred_lines(match):
		nonlocal file_label_counter
		# group 1 - @{!}<id>
		# group 2 - @{!}
		# group 3 - <id>
		# group 4 - rest of the instruction
		g2 = match.group(2)
		g3 = match.group(3)
		g4 = match.group(4)

		if 'bra' in g4 or 'brx' in g4:
			return match.group(0)

		if '!' in g2:
			g2 = '@'
		else:
			g2 = '@!'
		
		label_name = 'ad_hoc_L' + str(file_label_counter)
		file_label_counter += 1

		jump_line = g2 + g3 + ' bra ' + label_name + ';'
		inst_line = g4
		label_line = label_name + ':'

		whole_subst = jump_line + '\n' + inst_line + '\n' + label_line

		return whole_subst
	
	ptx_file_content = re.sub(ptx.r_pred_preprocess_pred_lines, substitute_pred_lines, ptx_file_content)

	# now, another task - we basically want to add label after each
	# call and branch instructions ... this way, all of the basic blocks
	# of PTX file will start with a basic block (by default PTX does not adhere 
	# to this)
	#
	# this operation greatly simplifies handling of calls and branches in the
	# XFG building phase

	# so, first branches
	# setp.lt.s32  p, i, n;
	# @!p bra L1;
	# add.s32   j, j, 1;
	# L1:
	# ...
	# =====>
	# setp.lt.s32  p, i, n;
	# @!p bra L1;
	# AD_HOC_L:
	# add.s32   j, j, 1;
	# L1:
	# ...

	def add_label_line(match):
		nonlocal file_label_counter
		label_name = 'ad_hoc_L' + str(file_label_counter)
		file_label_counter += 1

		label_line = label_name + ':'
		return match.group(0) + '\n' + label_line

	ptx_file_content = re.sub(ptx.r_pred_preprocess_branch_lines, add_label_line, ptx_file_content)

	# now calls
	ptx_file_content = re.sub(ptx.r_call_preprocess_call_lines, add_label_line, ptx_file_content)

	return ptx_file_content

def remove_operand_qualifiers(ptx_file_content):
	# There are two basic types of operand qualifiers that we want
	# to remove ... first, there are x, y, z, w and r, g, b, a
	# qualifiers that can be used with any vector register
	ptx_file_content = re.sub(ptx.rc_vec_idxs, '', ptx_file_content)

	def sub(match_group):
		match = match_group.group(0)
		return re.sub(ptx.rc_vec_qs, '', match)

	# we also want to remove qualifiers for vector instructions
	# like with vadd, vsub, vabsdiff instructions and so on
	ptx_file_content = re.sub(ptx.rc_vid_all_sc, sub, ptx_file_content)

	return ptx_file_content

def perform_task_devmap_preprocessing(ptx_file_content):
	# with devmap task (heterogenous device mapping), there are two changes that have to be performed
	# 1. during compilation, we artificially added function __stack_chk_fail
	# into each ptx file ... this function, along with its calls needs to be removed
	# 2. there are __stack_chk_guard constants ... they are replaced with constant 1

	ptx_file_content = re.sub('__stack_chk_guard\\b', '1', ptx_file_content)

	ptx_file_content = re.sub('\.func[ \t]*__stack_chk_fail.*?\}','', ptx_file_content, flags=re.S)

	ptx_file_content = re.sub('call\.uni[ \t]*__stack_chk_fail.*;', '', ptx_file_content)

	return ptx_file_content

def remove_empty_lines(ptx_file_content):
	# during preprocessing, we sometimes end up with a lot
	# of empty lines .. remove them
	ptx_file_content = re.sub('\n\n\n*', '\n', ptx_file_content)
	
	return ptx_file_content

def normalize_insts_single_line(f_line):
	# this function normalizes a single instruction line

	def normalize_single_inst(match):
		# 1st group: instruction
		# 3rd group: spaces between inst and operands
		# 4th group: operands
		inst = match.group(1)
		inst = re.sub('\s', '', inst)

		space = match.group(3)
		space = '\t'

		operands = match.group(4)
		operands = re.sub('\s*,', ',', operands)
		operands = re.sub(',\s+', ', ', operands)
		# if there are any other additional spaces left, like in memory addressing
		# then simply substitute them with one space
		operands = re.sub('\s+', ' ', operands)
		operands = re.sub('\s;', ';', operands)

		# instruction with no operands, like ret
		if operands == ';':
			return inst + operands

		return inst + space + operands

	# we dont care about labels
	if ':' in f_line:
		return f_line

	# we dont care about allocation
	if re.match('^\.', f_line) is not None:
		return f_line

	if re.match('^[a-z]', f_line):
		f_line = re.sub(ptx.rc_norm_1, normalize_single_inst, f_line)

	# predicated branch
	if re.match('^@', f_line):
		match = re.match(ptx.rc_norm_2, f_line)

		predicate = match.group(1)

		space = match.group(2)

		rest = re.sub(ptx.rc_norm_3, normalize_single_inst, match.group(3))

		f_line =  predicate + ' ' + rest

	return f_line

def normalize_instructions(ptx_file_content):
	# we can reduce vocabulary size (and hence quality of embedding vectors)
	# by transforming instructions into some normalized form 
	# for example, because of inline assembler, we may instructions like
	# add . u16        d , a   , b;
	# and we want to normalize it to:
	# add.u16<tab>d, a, b;
	# 
	# in addition, by separating each instruction op code and its operands
	# with tab, we can simplify the XFG building phase 

	ptx_file_lines = ptx_file_content.splitlines()

	ptx_file_lines = utils.transform_function_bodies(ptx_file_lines, normalize_insts_single_line)

	ptx_file_content = '\n'.join(ptx_file_lines)

	return ptx_file_content

def remove_semicolons(ptx_file_content):
	# we do not need semicolons in the following phases
	ptx_file_content = re.sub(';', '', ptx_file_content)

	return ptx_file_content

def perform_cosmetic_changes(ptx_file_content):
	# few minor cosmetic changes at the end
	ptx_file_content = re.sub('\A\n+', '', ptx_file_content)
	ptx_file_content = re.sub('\s+\Z', '', ptx_file_content)

	return ptx_file_content

def preprocess_core(ptx_file_content):
	# first two easy and straightforward tasks of removing comments
	# and useless directives
	ptx_file_content = remove_comments(ptx_file_content)
	ptx_file_content = remove_unnecessary_directives(ptx_file_content)

	# now, we will do some statement collapsing
	ptx_file_content = collapse_statements(ptx_file_content)
	
	# we can remove the forward declared functions
	ptx_file_content = remove_fwd_declared_functions(ptx_file_content)

	# removing variable initializers
	ptx_file_content = remove_var_initialization(ptx_file_content)

	# we need to uncollapse some statements
	ptx_file_content = uncollapse_function_scopes(ptx_file_content)

	# now we replace all of the parametrized variable declarations
	# with their unwinded versions
	ptx_file_content = replace_parametrized_variable_declarations(ptx_file_content)

	# replace few compile time constants (currently, only WARP_SZ)
	ptx_file_content = replace_compile_time_constants(ptx_file_content)

	# we also need to perform some operations on labels
	ptx_file_content = preprocess_labels(ptx_file_content)

	# now, preprocessing of predicated executions
	ptx_file_content = add_ad_hoc_labels(ptx_file_content)

	# remove leading and trailing spaces
	ptx_file_content = strip_lines(ptx_file_content)
	
	# we have to remove empty lines before mangling ... it can happen
	# that there are some preceeding empty lines before function body
	ptx_file_content = remove_empty_lines(ptx_file_content)

	# now we mangle identifiers of scoped variables
	ptx_file_content = mangle_scoped_identifiers(ptx_file_content)

	# remove vector indexing
	ptx_file_content = remove_operand_qualifiers(ptx_file_content)

	# in some cases, we need to perform some preprocessing that
	# is specific for the task we will perform
	ptx_file_content = perform_task_devmap_preprocessing(ptx_file_content)

	# we also want to remove empty lines inside the function
	ptx_file_content = remove_empty_lines(ptx_file_content)

	# we can simplify the subsequent stages by normalizing instructions
	ptx_file_content = normalize_instructions(ptx_file_content)

	# remove all semicolons -- there are not needed anymore
	ptx_file_content = remove_semicolons(ptx_file_content)

	# some cosmetic changes for improved readability
	ptx_file_content = perform_cosmetic_changes(ptx_file_content)	

	#print(ptx_file_content)
	#exit()

	return ptx_file_content


#####################################
####### PUBLIC API
#####################################
def preprocess(input_data_folder, generated_files_folder, restore):
	# this is the main public function of preprocesser; 
	# it goes over each directory in input_data_folder, and for each
	# such directory, it goes over all of the PTX files, preprocesses
	# them and outputs them into the respective folder under generated_files_folder
	# with "_pre.ptx" suffix
	#
	# also, as this takes a considerable amount of time for ptx2vec embedding space
	# training, this function is able to restore the preprocessing from a checkpoint

	utils.report_phase_started('preprocessing')

	# here, when restore flag is set to false, we remove all of the generated
	# folders (these contain only files that are related to this phase ...)
	# training data and vocabulary are only two files in the emb_gen_files_folder
	# and embeddings are in a separate folder 
	if restore == False:
		utils.remove_all_folders(generated_files_folder)
	
	for dir_name, f_name, ptx_file_content in utils.read_files(input_data_folder, \
		'.ptx', utils.read_text_f):

		ptx_pre_name = f_name[:-4] + '_pre.ptx'
		ptx_pre_path = os.path.join(generated_files_folder, dir_name, ptx_pre_name)

		if restore and os.path.exists(ptx_pre_path):
			continue
		
		# as preprocessing is the first step, we might need to create
		# the directories
		if not os.path.exists(os.path.dirname(ptx_pre_path)):
			os.makedirs(os.path.dirname(ptx_pre_path))

		ptx_file_content = preprocess_core(ptx_file_content)
		utils.write_text_f(ptx_pre_path, ptx_file_content)