# This file implements XFG. XFG is a graph defined by Ben-Nun et al.
# (see http://papers.nips.cc/paper/7617-neural-code-comprehension-a-learnable-representation-of-code-semantics)
# that captures both data and control dependencies in a graph, which allows for
# an easy detection of instruction context, which is in turned used for generation
# of training data for the Skip-Gram model. (note however, that our implementation
# slightly differs from the XFGs used in the aforementioned paper)
#
# The most tricky part related to XFGs is their building. This step is performed in the
# XFG building phase that is implemented in xfg_builder.py
# 
# The building process is quite complicated and understanding it requires a good
# grasp of the PTX instruction set. Essentially, there are four main types 
# of instructions that can be used to implement almost all PTX instructions. These
# instruction types are: generic register instructions, load instructions,
# store instructions and control flow instructions.
# 
# For each of these instruction types, there is a dedicated function that handles
# adding of this instruction into the graph (for control flow instructions, there
# are actually several functions). See the respective functions for further information
# about handling of the respective instruction types.

import networkx as nx
from typing import List
from enum import Enum
from dataclasses import dataclass

from src_common import utils

class op_type(Enum):
	imm = 1
	sreg = 2
	var = 3
	reg = 4

@dataclass
class classified_op:
	op_t : op_type
	op_id : str

	def __iter__(self):
		return iter((self.op_t, self.op_id))

class XFG:
	# ad_hoc edge types
	edge_label_ctrl_pass = 'ctrl'

	# special nodes
	function_ret_node = 'ret'
	kernel_exit_node = 'exit'

	########################
	#### NODES
	########################
	def _add_node(self, node : str):
		if self._G.has_node(node):
			utils.error(f'adding node {node} for the second time')
		self._G.add_node(node)

	def _add_ad_hoc_node(self) -> str:
		ad_hoc = 'ad_hoc_' + str(self._ad_hoc_cntr)
		self._ad_hoc_cntr += 1
		self._add_node(ad_hoc)
		return ad_hoc
	
	def _has_node(self, node):
		return self._G.has_node(node)

	########################
	#### EDGES
	########################

	def _add_stmnt_on_hold(self, src: str, dest : str, label : str):
		self._stmnts_on_hold.append((src, dest, label))

	def _add_edge(self, src : str, dest : str, label : str):	
		if not self._G.has_node(src):
			self._add_stmnt_on_hold(src, dest, label)
			return

		if not self._G.has_node(dest):
			utils.warning(f'_add_edge: g does not have node {dest} in stmt: {label}')
			self._add_node(dest)

		if self._has_edge(src, dest):
			if label in self.get_all_edge_labels(src,dest):
				return 
			
		self._G.add_edge(src, dest, label=label)

	def _has_edge(self, src : str, dest : str):
		if self._G.has_edge(src, dest):
			return True
		return False


	########################
	#### PUBLIC API
	########################

	########################
	#### MOST USED XFG BUILDING CASES
	########################

	def add_reg_inst(self, dest_reg : str, src_c_ops : List[classified_op], stmt):
		self._last_working_node = dest_reg
		# whenever we have a register as destination, the rest of this
		# function makes sure that it will be reachable from the current 
		# block node (either via direct edge, or via other nodes)
		# we can add it to visited nodes now
		self._visited_nodes.add(dest_reg)

		if not self._has_node(dest_reg):
			self._add_node(dest_reg)

			# if there is not any register dependency, then add
			# edge from the current block node 
			if not any(src_t == op_type.reg for src_t, _ in src_c_ops):
				self._add_edge(self._current_block_node, dest_reg, stmt)
			else:
				# if there is a register dependency, then first, we 
				# add all of the register edges
				for src_t, src_id in src_c_ops:
					if src_t == op_type.reg:
						self._add_edge(src_id, dest_reg, stmt)
				
				# now, let'S check if one of the source registers is in visited
				# nodes ... if yes, then the dest_reg is already reachable from
				# current block node, and there is no need to add the edge
				if not any(src_id in self._visited_nodes for _, src_id in src_c_ops):
					self._add_edge(self._current_block_node, dest_reg, stmt)

		else:
			# the node is already present ...

			# if there is not any register dependency, then we simply
			# add a new edge from current block node
			if not any(src_t == op_type.reg for src_t, _ in src_c_ops):
				# add edge from the current block node to this node
				self._add_edge(self._current_block_node, dest_reg, stmt)
			else:
				# else ... we have some register dependency

				# here, we also need to handle a special cases with self-referring instructions
				# like remm x8, x8, 2;

				# if there is no such case, then ...
				if not any(src_t == op_type.reg and src_id == dest_reg for src_t, src_id in src_c_ops):
					# with normal register dependencies, we first 
					# add all of the register edges
					for src_t, src_id in src_c_ops:
						if src_t == op_type.reg:
							self._add_edge(src_id, dest_reg, stmt)
				else:
					# we know that we have a self-referring instruction

					# if there are any other registers in the instruction
					# then we proceed as before
					if any(src_t == op_type.reg and src_id != dest_reg for src_t, src_id in src_c_ops):
						for src_t, src_id in src_c_ops:
							if src_t == op_type.reg and src_id != dest_reg:
								self._add_edge(src_id, dest_reg, stmt)
					else:
						# we add ad_hoc node and edge
						ad_hoc_node = self._add_ad_hoc_node()
						self._add_edge(ad_hoc_node, dest_reg, stmt)
				
				if not any(src_id in self._visited_nodes for src_t, src_id in src_c_ops):
					self._add_edge(self._current_block_node, dest_reg, stmt)


	def add_st_inst(self, dest_c_ops : List[classified_op], src_regs : List[str], stmt):
		# handler of generic store instruction

		ad_hoc_node = self._add_ad_hoc_node()
		self._last_working_node = ad_hoc_node
		# here, there is no need to add ad_hoc_node to a visited nodes
		# set ... it is never considered as source in the generic register inst

		for src_reg in src_regs:
			self._add_edge(src_reg, ad_hoc_node, stmt)

		for dest_t, dest_id in dest_c_ops:
			if dest_t == op_type.reg:
				self._add_edge(dest_id, ad_hoc_node, stmt)
				src_regs.append(dest_id)
		
		if not any(src_reg in self._visited_nodes for src_reg in src_regs):
			self._add_edge(self._current_block_node, ad_hoc_node, stmt)
		

	def add_ld_inst(self, dest_regs : List[str], src_c_ops : List[classified_op], stmt):
		# after several iterations, it turns out that 
		# we can use the reg function for ld instruction types
		for dest_reg in dest_regs:
			self.add_reg_inst(dest_reg, src_c_ops, stmt)
			
	########################
	#### SPECIAL XFG BUILDING CASES
	########################
	def add_to_label_ctrl_pass(self, to_label, stmt):
		# handler of branch instruction
		self._add_edge(self._last_working_node, to_label, stmt)
	
	def add_ret_inst(self, stmt):
		# return instruction
		self._add_edge(self._last_working_node, XFG.function_ret_node, label=stmt)

	def add_exit_inst(self, stmt):
		# exit instruction
		self._G.add_edge(self._last_working_node, XFG.kernel_exit_node, label=stmt)
		
	def add_dep_from_last_working_node(self, src_c_ops : List[classified_op], stmt):
		# special case used in several instructions that do not belong to
		# any of the typical cases
		ad_hoc_node = self._add_ad_hoc_node()	

		self._add_edge(self._last_working_node, ad_hoc_node, stmt)

		for src_t, src_id in src_c_ops:
			if src_t == op_type.reg:
				self._add_edge(src_id, ad_hoc_node, stmt)
		
		self._last_working_node = ad_hoc_node

	def set_label_context_discontinous(self, label_name):
		# new label encountered - case with unconditional jump before the label
		self._last_working_node = label_name
		self._current_block_node = label_name
		self._visited_nodes = set()

	def set_label_context_continuous_call(self, label_name, call_stmnt):
		# new label encountered - preceeding call statement
		self._add_edge(self._last_working_node, label_name, call_stmnt)
		self._last_working_node = label_name
		self._current_block_node = label_name
		self._visited_nodes = set()

	def set_label_context_continuous(self, label_name):
		# new label encountered - case where the preceeding instruction
		# is not an unconditional jump
		self._add_edge(self._last_working_node, label_name, XFG.edge_label_ctrl_pass)
		self._last_working_node = label_name
		self._current_block_node = label_name
		self._visited_nodes = set()

	def set_function_context(self, function_name):
		# intialization function
		self._add_node(function_name)
		self._add_node(XFG.function_ret_node)
		self._current_block_node = function_name
		self._last_working_node = function_name
		self._stmnts_on_hold = []
		self._visited_nodes = set()

	def add_label_node(self, label_name):
		self._add_node(label_name)

	def finalize(self):
		tmp = self._stmnts_on_hold
		self._stmnts_on_hold = []
		if len(tmp) > 0:
			utils.warning(f'adding {len(tmp)} stmnts on hold')

		for src, dest, label in tmp:
			self._add_edge(src, dest, label)

		if len(self._stmnts_on_hold) != 0:
			utils.warning(f'was not able to add all stmnts on hold {len(self._stmnts_on_hold)} remain')
			if len(self._stmnts_on_hold) == 1:
				print('examples:')
				print(self._stmnts_on_hold[0])
			if len(self._stmnts_on_hold) > 3:
				print(self._stmnts_on_hold[1])
				print(self._stmnts_on_hold[2])

	########################
	#### STATIC FUNCTIONS
	########################

	def is_empty_edge(edge_label):
		if edge_label.startswith(XFG.edge_label_ctrl_pass):
			return True
		return False

	########################
	#### OTHER PUBLIC FUNCTIONS
	########################
	
	def nodes(self):
		return self._G.nodes()

	def edges(self):
		return self._G.edges()

	def edges_it(self):
		# function for iterating over the edges and their labels
		for v1, v2, edge_id, data in self._G.edges(keys = True, data = True):
			label = data['label']
			yield (v1, v2, edge_id, label)

	def get_all_edge_labels(self, v1, v2):
		edge_labels = []
		for edge_id, data in self._G[v1][v2].items():
			edge_labels.append(data['label'])
		return edge_labels

	def set_edge_label(self, v1, v2, edge_id, label):
		self._G[v1][v2][edge_id]['label'] = label

	def G(self):
		return self._G

	def __init__(self):	
		self._G = nx.MultiDiGraph()
		self._current_block_node = ''
		self._last_working_node = ''
		self._ad_hoc_cntr = 0
		self._add_node(XFG.kernel_exit_node)
		self._stmnts_on_hold = []