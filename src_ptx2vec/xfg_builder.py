# This file implements the XFG building phase of ptx2vec.
#
# This phase takes all of the preprocessed PTX files from preprocesser
# and it builds a separate XFG for each function inside this file
# XFG is a graph defined by Ben-Nun et al.
# (see http://papers.nips.cc/paper/7617-neural-code-comprehension-a-learnable-representation-of-code-semantics)
# that captures both data and control dependencies in a graph, which allows for
# an easy detection of instruction context, which is in turned used for generation
# of training data for the Skip-Gram model.

import os
import re
import pickle
from dataclasses import dataclass
from typing import Tuple

from src_common import utils
from src_ptx2vec import ptx_info as ptx
from src_ptx2vec import xfg_graph
from src_ptx2vec import xfg_context
from src_ptx2vec.xfg_graph import op_type
from src_ptx2vec.xfg_graph import classified_op

@dataclass
class inst_context:
	line : str
	inst : str
	ops : str

def classify_inst_op(op_part : str, g_context, f_context) -> xfg_graph.classified_op:
	# during the handling of a particular instruction, we need to classify 
	# the instruction operands as is expected by the XFG class
	#
	# this is an important step, because it allows us to detect if a particular
	# operand is register, variable and others

	if re.search(ptx.r_variable_identifier, op_part) is None:
		if re.search(ptx.any_of(ptx.r_consts), op_part) is None:
			print(op_part)
		imm_val = re.search(ptx.any_of(ptx.r_consts), op_part).group(0)
		return classified_op(op_type.imm, imm_val)
	else:
		op_id = re.search(ptx.r_variable_identifier, op_part).group(0)
		
		if op_id in ptx.sreg_ids:
			return classified_op(op_type.sreg, op_id)
		# note that in mov instruction, we may also store things like labels or function
		# names into the register
		elif op_id in f_context.other_vars or op_id in g_context.global_vars or \
			op_id in f_context.params or \
			op_id in g_context.function_names or op_id in f_context.labels:
			return classified_op(op_type.var, op_id)
		else:
			if not op_id in f_context.regs:
				utils.error('can not classify operand: ' + op_id + ' ... it does not belong to any of the categories')
			return classified_op(op_type.reg, op_id)

#####################################
####### GENERIC INSTRUCTION HANDLERS
#####################################

def handle_generic_register_inst(xfg, i_context, g_context, f_context):
	# this function is capable of handling generic register instructions
	# with multiple source operands and multiple destination operands
	# in general, it can be used on any instruction that has 
	# some destination registers and >= 1 source operands
	ops_split = i_context.ops.split(',')

	dest_regs = []
	if '|' in ops_split[0]:
		# two destination register, like d1 | d2, s1, s2, s3, ...;
		dest_split = ops_split[0].split('|')
		dest_regs.append(re.search(ptx.r_variable_identifier, dest_split[0]).group(0))
		dest_regs.append(re.search(ptx.r_variable_identifier, dest_split[1]).group(0))
	elif '{' in ops_split[0]:
		# vector register instruction
		for i in range(0, len(ops_split)):
			dest_regs.append(re.search(ptx.r_variable_identifier, ops_split[i]).group(0))
			if '}' in ops_split[i]:
				break
	else:
		# we have only one destination register
		dest_regs.append(re.search(ptx.r_variable_identifier, ops_split[0]).group(0))

	for dest_reg in dest_regs:
		src_c_ops = []
		for op_part in ops_split[len(dest_regs):]:
			src_c_ops.append(classify_inst_op(op_part, g_context, f_context))
		xfg.add_reg_inst(dest_reg, src_c_ops, i_context.line)

def handle_generic_load_inst(xfg, i_context, g_context, f_context):
	# generic load instruction that takes the destination
	# registers and uses the remaining part of the instruction
	# as sources for loading ...
	# used in the following instrucitons: normal ld,
	# texture instructions like tex, surface instructions like suld
	# and wmma load instructions
	
	ops_split = i_context.ops.split(',')

	# many instructions have vector register as they destination
	dest_regs = []
	if '{' in ops_split[0]:
		for i in range(0, len(ops_split)):
			dest_regs.append(re.search(ptx.r_variable_identifier, ops_split[i]).group(0))
			if '}' in ops_split[i]:
				break
	else:
		dest_regs.append(re.search(ptx.r_variable_identifier, ops_split[0]).group(0))

	src_c_ops = []
	for addr_src_op in ops_split[len(dest_regs):]:
		src_c_ops.append(classify_inst_op(addr_src_op, g_context, f_context))

	xfg.add_ld_inst(dest_regs, src_c_ops, i_context.line)

def handle_generic_store_inst(xfg, i_context, g_context, f_context):
	# used in the following instructions:
	# normal st, surface instructions sured and sust and lastly wmma.store

	ops_split = i_context.ops.split(',')

	# surface store instruction can have more than one operand in
	# the destination address operand 
	dest_c_ops = []
	for i in range(0, len(ops_split)):
		dest_c_ops.append(classify_inst_op(ops_split[i], g_context, f_context))
		if ']' in ops_split[i]:
			break
	
	# this function expects that all of the source operands are registers
	src_regs = []
	for src_op in ops_split[len(dest_c_ops):]:
		src_regs.append(re.search(ptx.r_variable_identifier, src_op).group(0))

	xfg.add_st_inst(dest_c_ops, src_regs, i_context.line)

#####################################
####### HANDLING OF KERNEL LINES
#####################################

def handle_arithmetic_inst(xfg, i_context, g_context, f_context):
	handle_generic_register_inst(xfg, i_context, g_context, f_context)

def handle_cmpsel_inst(xfg, i_context, g_context, f_context):
	handle_generic_register_inst(xfg, i_context, g_context, f_context)

def handle_ls_inst(xfg, i_context, g_context, f_context):
	handle_generic_register_inst(xfg, i_context, g_context, f_context)

def handle_data_movement_and_comparison_inst(xfg, i_context, g_context, f_context):
	if re.match(ptx.any_of([ptx.r_dmc_shfl, ptx.r_dmc_prmt, ptx.r_dmc_cvt, ptx.r_dmc_cvta, \
		ptx.r_dmc_isspacep, ptx.r_dmc_mov]), i_context.inst):
		handle_generic_register_inst(xfg,i_context, g_context, f_context)
	elif re.match(ptx.any_of([ptx.r_dmc_ld, ptx.r_dmc_ldu]), i_context.inst):
		handle_generic_load_inst(xfg, i_context, g_context, f_context)
	elif re.match(ptx.r_dmc_st, i_context.inst):
		handle_generic_store_inst(xfg, i_context, g_context, f_context)

def handle_ctrl_inst(xfg, i_context, g_context, f_context):
	ops_split = i_context.ops.split(',')
	if re.search(ptx.r_cf_bra, i_context.inst):
		# unconditional/conditional bra (single label)
		label_name = re.search(ptx.r_variable_identifier, ops_split[0]).group(0)
		xfg.add_to_label_ctrl_pass(label_name, i_context.line)
	elif re.search(ptx.r_cf_brx, i_context.inst):
		# unconditional/conditional brx (multiple labels)
		# extract name of the label that holds potential branch targets
		src_label = re.search(ptx.r_variable_identifier, ops_split[0]).group(0)
		branch_targets = f_context.branch_targets[src_label]

		# and add the dependencies
		for tgt_label in branch_targets:
			xfg.add_to_label_ctrl_pass(tgt_label, i_context.line)
	elif re.match(ptx.r_cf_ret, i_context.inst):
		xfg.add_ret_inst(i_context.line)
	elif re.match(ptx.r_cf_exit, i_context.inst):
		xfg.add_exit_inst(i_context.line)
	elif re.match(ptx.r_cf_call, i_context.inst):
		# this is already cleverly handled with labels
		# (there is no edge from the function to other functions
		# during call instruction)
		pass

def handle_para_sync_insts(xfg, i_context, g_context, f_context):
	ops_split = i_context.ops.split(',')
	if re.match(ptx.r_psc_bar, i_context.inst):
		if 'red' in i_context.inst:
			# reduction case
			handle_generic_register_inst(xfg, i_context, g_context, f_context)
		else:
			# add edge from last working node to new ad_hoc node
			src_c_ops = [classify_inst_op(op, g_context, f_context) for op in ops_split]
			xfg.add_dep_from_last_working_node(src_c_ops, i_context.line)
	elif re.match(ptx.any_of([ptx.r_psc_membar, ptx.r_psc_fence]), i_context.inst):
		# add edge from last working node to new ad_hoc_node
		# these two instructions dont have any operand
		xfg.add_dep_from_last_working_node([], i_context.line)
	elif re.match(ptx.any_of([ptx.r_psc_vote, ptx.r_psc_match]), i_context.inst):
		handle_generic_register_inst(xfg, i_context, g_context, f_context)
	elif re.match(ptx.r_psc_amask, i_context.inst):
		dest_reg = re.search(ptx.r_variable_identifier, ops_split[0]).group(0)
		xfg.add_reg_inst(dest_reg, [], i_context.line)
	elif re.match(ptx.any_of([ptx.r_psc_atom, ptx.r_psc_red]), i_context.inst):
		# we can deal with the atomic and red instruction with kinda trick
		# by using combination of ld and st instructions

		# load
		dest_reg = re.search(ptx.r_variable_identifier, ops_split[0]).group(0)
		addr_c_op = classify_inst_op(ops_split[1], g_context, f_context)
		xfg.add_ld_inst([dest_reg], [addr_c_op], i_context.line)

		# store
		src_regs = [dest_reg]
		for i in range(2, len(ops_split)):
			src_c_op = classify_inst_op(ops_split[i], g_context, f_context)
			if src_c_op.op_t == op_type.reg:
				src_regs.append(src_c_op.op_id)
		xfg.add_st_inst([addr_c_op], src_regs, i_context.line)

def handle_tex_instructions(xfg, i_context, g_context, f_context):
	if re.match(ptx.any_of([ptx.r_tex_tex, ptx.r_tex_txq, ptx.r_tex_tld4]), i_context.inst):
		handle_generic_load_inst(xfg, i_context, g_context, f_context)
	if re.match(ptx.r_tex_istypep, i_context.inst):
		handle_generic_register_inst(xfg, i_context, g_context, f_context)

def handle_surf_instructions(xfg, i_context, g_context, f_context):
	if re.match(ptx.any_of([ptx.r_surf_suld, ptx.r_surf_suq]), i_context.inst):
		handle_generic_load_inst(xfg, i_context, g_context, f_context)
	elif re.match(ptx.any_of([ptx.r_surf_sust, ptx.r_surf_sured]), i_context.inst):
		handle_generic_store_inst(xfg, i_context, g_context, f_context)

def handle_vid_instructions(xfg, i_context, g_context, f_context):
	handle_generic_register_inst(xfg, i_context, g_context, f_context)

def handle_wmma_instructions(xfg, i_context, g_context, f_context):
	if re.match(ptx.any_of([ptx.r_wmma_load, ptx.r_mma_ldmatrix]), i_context.inst):
		handle_generic_load_inst(xfg, i_context, g_context, f_context)
	elif re.match(ptx.any_of([ptx.r_wmma_mma, ptx.r_mma_mma]), i_context.inst):
		handle_generic_register_inst(xfg, i_context, g_context, f_context)
	elif re.match(ptx.r_wmma_store, i_context.inst):
		handle_generic_store_inst(xfg, i_context, g_context, f_context)

def handle_misc_instructions(xfg, i_context, g_context, f_context):
	if re.match(ptx.r_misc_trap, i_context.inst):
		xfg.add_exit_inst(i_context.line)

def handle_label(xfg, label_line, g_context, f_context):
	# when we handle label, we would want to add an edge
	# from the previous working node to this label ... however,
	# we need to be careful about several cases; such edge does not 
	# make any sense if the previous instruction was an unconditional
	# branch, trap, exit and so on
	# in addition, we need to add ret dependency if the previous
	# instruction was call
	label_name = re.search(ptx.r_label, label_line).group(1)

	prev_line = f_context.last_line

	if 'call' in prev_line:
		xfg.set_label_context_continuous_call(label_name, prev_line)
		return

	disc_instructions = ['bra', 'brx', 'trap', 'exit', 'ret']
	
	if any(disc_inst in prev_line for disc_inst in disc_instructions):
		if not '@' in prev_line:
			xfg.set_label_context_discontinous(label_name)
		else:
			xfg.set_label_context_continuous(label_name)
	else:
		xfg.set_label_context_continuous(label_name)


#####################################
####### BUILD XFG - SINGLE FUNCTION
#####################################
def build_xfg_single_f(g_context, f_def_line, f_body_lines):
	# this function builds XFG for one kernel that is passed
	# as a list of lines to this function
	#
	# the algorithm for building XFG is quite complicated;
	# essentially, we go over each instruction in the function body, we
	# classify it (is it an arithmetic istrunction or data movement instruction ?)
	# and then the handling of that particular type of instruction is delegated
	# to its own function (these are the functions handle_xxxx_instructions);
	#
	# handling of the instruction essentially boils down to handling of four different
	# intruction types: generic register instructions (like add), load instruction,
	# store instructions and control flow instructions (and a few other edge-cases)
	# for these types of instructions, the XFG class contains a suitable function
	# that adds this type of instruction into the graph - see xfg_graph.py for 
	# further information about building XFG

	# first, collect function context for this xfg (see xfg_context for 
	# explanation of function context)
	f_context = xfg_context.collect_f_context(f_def_line, f_body_lines)	

	# create an empty XFG
	xfg = xfg_graph.XFG()

	# and add few starting nodes, mainly function node and labels
	xfg.set_function_context(f_context.f_name)
	for label in f_context.labels:
		xfg.add_label_node(label)

	idx = 0
	# now go over the whole f body
	for line in f_body_lines:
		# we do not care about local variables
		if re.match(ptx.r_local_variable, line):
			pass
		elif re.match(ptx.r_label, line):
			handle_label(xfg, line, g_context, f_context)
		else:
			# we have some instruction
			inst_split = line.split('\t')
			inst = inst_split[0]

			# get the operands (there might be none)
			ops = ''
			if len(inst_split) > 1:
				ops = inst_split[1]

			# create instruction context from the op code and operands
			i_context = inst_context(line, inst, ops)
			
			# and based on the type of instruction (arithmetic, data movement, ...)
			# invoke the corresponding handler of this type of instruction
			if re.match(ptx.rc_a_all, inst):
				handle_arithmetic_inst(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_cmpsel_all, inst):
				handle_cmpsel_inst(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_ls_all, inst):
				handle_ls_inst(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_dmc_all, inst):
				handle_data_movement_and_comparison_inst(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_cf_all, inst):
				handle_ctrl_inst(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_psc_all, inst):
				handle_para_sync_insts(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_tex_all, inst):
				handle_tex_instructions(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_surf_all, inst):
				handle_surf_instructions(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_wmma_all, inst):
				handle_wmma_instructions(xfg, i_context, g_context, f_context)			
			elif re.match(ptx.any_of(ptx.r_misc_insts), inst):
				handle_misc_instructions(xfg, i_context, g_context, f_context)
			elif re.match(ptx.rc_vid_all, inst):
				handle_vid_instructions(xfg, i_context, g_context, f_context)
			else:
				print(f_context.last_line)
				utils.error('can not handle line: ' + line + ' while processing function body ... ' + str(line))

		f_context.last_line = line
		idx += 1
		if idx % 1000 == 0:
			utils.report_xfg_building_progress(idx, len(f_body_lines))

	# finalize the xfg
	xfg.finalize()
	
	return f_context, xfg


#####################################
####### PUBLIC API
#####################################
def build_xfgs(generated_files_folder, restore):
	# this is the main function of XFG building phase - it goes over each
	# preprocessed file that was generated by the preprocessing phase
	# (these files are present in the generated files folder) and
	# for each file, it builds an XFG for each kernel present in this file
	#
	# for additional steps, see function build_xfg_single_f
	utils.report_phase_started('building xfgs')

	for dir_name, file_name, pre_ptx_file_content in utils.read_files(generated_files_folder, \
		'_pre.ptx', utils.read_text_f):

		# collect global context of this file and "list" of functions
		# in this file ... see xfg_context.py for explanation of context
		g_context = xfg_context.collect_g_context(pre_ptx_file_content.splitlines())
		functions = utils.collect_functions(pre_ptx_file_content.splitlines())

		# now go over each of these functions and build XFG for each one of them
		idx = 0
		for f_def_line, _, f_body_lines, _ in functions:
			xfg_name = file_name[:-8] + '_' + str(idx) + '_xfg.p'
			xfg_path = os.path.join(generated_files_folder, dir_name, xfg_name)

			utils.report_xfg_building(idx, len(functions))
			idx += 1
			if restore and os.path.exists(xfg_path):
				continue
		
			f_context, xfg = build_xfg_single_f(g_context, f_def_line, f_body_lines)
			# there might be kernels with only very few nodes (like
			# empty kernel with only ret instruction) - we dont want these
			if len(xfg.nodes()) < 5:
				continue

			# lastly, save the XFG, its global context and the corresponding
			# function context for the next phases
			utils.pickle_xfg_w(xfg_path, g_context, f_context, xfg)
			del xfg
