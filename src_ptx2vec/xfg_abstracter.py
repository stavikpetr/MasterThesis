# This file implements the XFG abstraction phase of ptx2vec.
#
# This module is used in two contexts. Primarily, it is used
# for transforming each instruction of XFG into abstract form.
# For example, instruction add.u32 r0, r1, r2; is transformed
# into add.u32 <REG>, <REG>, <REG>;
# This step is necessary because otherwise, two instructions
# that differ only in the used registers would be assigned, in the
# vocabulary building phase, two different vocabulary indices.
# This functionality is implemented in the abstract_xfgs function.
# 
# Secondly, this module is also used in each task. The reason is,
# that in each task, we have to again perform this mapping of instructions
# into their abstract form. Otherwise, it would not be possible
# to map an instruction of a particular kernel in the dataset into
# its vocabulary index, becaues the vocabulary consists of abstract instructions.
# This functionality is implemented in the abstract_ptxs function.
# 
# There are two abstraction granularities implemented. First, "finest" granularity
# is used in the ptx2vec approach, while "coarse" granularity is used in the
# ptx_ab approach.

import os
import re
import pickle
import json

from src_common import utils
from src_ptx2vec import xfg_graph
from src_ptx2vec import xfg_context
from src_ptx2vec import ptx_info as ptx

#####################################
####### FINEST ABSTRACTION OF INSTRUCTIONS
#####################################

def abstract_operands_finest(g_context, f_context, operands):
	# abstraction of operands with the finest abstraction granularity

	def subsitute_variable_identifier(match):
		identifier = match.group(0)

		if identifier in f_context.regs:
			return '<REG>'
		elif identifier in ptx.sreg_ids:
			return '<SREG>'
		elif identifier in g_context.global_vars:
			return '<G_VAR>'
		elif identifier in g_context.ext_function_names:
			return '<EXT_FUNC>'
		elif identifier in g_context.function_names:
			return '<FUNC>'
		elif identifier in f_context.params:
			return '<PARAM>'
		elif identifier in f_context.other_vars:
			return '<L_VAR>'
		elif identifier in f_context.labels:
			return '<LBL>'
		
		utils.error(f'can not substitute variable identifier of: {identifier} while abstracting operands: {operands}')
		
	operands = re.sub(ptx.rc_variable_identifier, subsitute_variable_identifier, operands)

	operands = re.sub(ptx.rc_consts_f, '<FLOAT>', operands)

	operands = re.sub(ptx.rc_consts_ip, '<INT>', operands)

	return operands

def abstract_op_code_finest(g_context, f_context, op_code):
	# abstraction of op code with the finest abstraction granularity
	if op_code[0] == '@':
		op_code_split = op_code.split(' ')
		abst_left = abstract_operands_finest(g_context, f_context, op_code_split[0])
		return abst_left + ' ' + op_code_split[1]
	
	return op_code

def abstract_inst_finest(g_context, f_context, inst):
	# abstract a single instruction with the finest granularity
	inst_split = inst.split('\t')
	
	op_code = inst_split[0]
	# abstract op code
	ab_op_code = abstract_op_code_finest(g_context, f_context, op_code)

	# instruction with no operands
	if len(inst_split) == 1:
		return ab_op_code

	operands = inst_split[1]

	# if the instruction has some operands, then also abstract the operands
	ab_operands = abstract_operands_finest(g_context, f_context, operands)

	return ab_op_code + '\t' + ab_operands

#####################################
####### COARSE ABSTRACTION OF INSTRUCTIONS
#####################################

def abstract_op_code_coarse(op_code):
	# abstraction of operands with the coarse abstraction granularity
	if op_code[0] == '@':
		op_code = op_code.split(' ')[1]

	op_code_split = op_code.split('.')
	ab_op_code = op_code_split[0]

	if '.shared' in op_code:
		ab_op_code += '.shared'
	elif '.const' in op_code:
		ab_op_code += '.const'
	elif '.global' in op_code:
		ab_op_code += '.global'
	elif '.local' in op_code:
		ab_op_code += '.local'
	elif '.param' in op_code:
		ab_op_code += '.param'

	if '.f16' in op_code or '.f32' in op_code or '.f64' in op_code:
		ab_op_code += '.f'

	return ab_op_code

def abstract_inst_coarse(g_context, f_context, inst):
	# abstract a single instruction with the coarse granularity
	inst_split = inst.split('\t')

	op_code = inst_split[0]
	ab_op_code = abstract_op_code_coarse(op_code)

	return ab_op_code

##############################
##############################

def abstract_xfg(xfg, g_context, f_context, abstract_func):
	# abstract XFG by going over its edges and abstracting
	# each instruction
	for v1, v2, edge_id, label in xfg.edges_it():
		if xfg_graph.XFG.is_empty_edge(label):
			continue
		new_label = abstract_func(g_context, f_context, label)
		xfg.set_edge_label(v1, v2, edge_id, new_label)

	ab_edges = [l for _, _, _, l in xfg.edges_it() if not xfg_graph.XFG.is_empty_edge(l)]

	return xfg, '\n'.join(ab_edges)

def get_abstract_func(ab_granularity):
	if ab_granularity == 'finest':
		return abstract_inst_finest
	elif ab_granularity == 'coarse':
		return abstract_inst_coarse

#####################################
####### PUBLIC API
#####################################

def abstract_xfgs(generated_files_folder, ab_granularity, restore):
	# this is the main function that performs the XFG abstraction phase
	# which transforms each instruction in the XFG into its abstract form
	#
	# it goes over each XFG file in the generated_files_folder and for each
	# edge in the XFG, it performs the abstraction
	#
	# in addition, it outputs all of the abstract instructions in an XFG into
	# ab_stmnts file ... these files are used in the vocabulary building phase
	# for building of the vocabulary

	utils.report_phase_started('abstracting xfgs')

	for dir_name, f_name, xfg_tuple in utils.read_files(generated_files_folder,
		'_xfg.p', utils.pickle_xfg_r):
		g_context, f_context, xfg = xfg_tuple

		xfg_ab_name = f_name[:-6] + '_xfg_ab.p'
		xfg_ab_path = os.path.join(generated_files_folder, dir_name, xfg_ab_name)

		if restore and os.path.exists(xfg_ab_path):
			continue

		ab_stmnts_fname = f_name[:-6] + '_ab_stmnts.txt'
		ab_stmnts_fp = os.path.join(generated_files_folder, dir_name, ab_stmnts_fname)

		# get the abstraction functor for each edge/instruction based on the
		# abstraction granularity
		abstract_func = get_abstract_func(ab_granularity)

		# perform the abstraction
		ab_xfg, ab_edges = abstract_xfg(xfg, g_context, f_context, abstract_func)

		# and pickle the XFG and its abstract instructions for the following phases
		utils.pickle_xfg_w(xfg_ab_path, g_context, f_context, xfg)
		utils.write_text_f(ab_stmnts_fp, ab_edges)

def abstract_ptxs(generated_files_folder, ab_granularity):
	# this function is invokes exclusively from the implementation of the final
	# tasks; as already noted, it transforms each instruction of the task
	# training data into its abstract form so that it can be mapped to a vocabulary
	# index and then encoded
	#
	# it goes over each preprocessed file and it abstracts each instruction
	# that is present in this file with either finest granularity (ptx2vec)
	# or coarse granularity (ptx_ab)

	utils.report_phase_started('abstracitng ptx files')

	for dir_name, f_name, ptx_file_content in utils.read_files(generated_files_folder,
		'_pre.ptx', utils.read_text_f):

		ptx_file_lines = ptx_file_content.splitlines()

		g_context = xfg_context.collect_g_context(ptx_file_lines)
		functions = utils.collect_functions(ptx_file_lines)

		# get the abstraction functor for each instruction based on the
		# abstraction granularity (coarse vs finest)
		abstract_func = get_abstract_func(ab_granularity)

		# go over each instruction in the function and perform abstraction
		for f_def_line, _, f_body_lines, f_body_start_idx in utils.collect_functions(ptx_file_lines):
			f_context = xfg_context.collect_f_context(f_def_line, f_body_lines)
			def abstract_single_line(inst_line):
				if ':' in inst_line:
					return ''
				if re.match('^\.', inst_line) is not None:
					return ''

				return abstract_func(g_context, f_context, inst_line)

			utils.transform_function_body(f_body_lines, abstract_single_line)

			ptx_file_lines[f_body_start_idx:f_body_start_idx + len(f_body_lines)] = f_body_lines

		ptx_file_content = '\n'.join(ptx_file_lines)

		# perform several special substitutions in the PTX file
		ptx_file_content = re.sub('^\..*', '', ptx_file_content, flags=re.M)
		ptx_file_content = re.sub('^\{', '', ptx_file_content, flags=re.M)
		ptx_file_content = re.sub('^\}', '', ptx_file_content, flags=re.M)
		ptx_file_content = re.sub('\n\n\n*', '\n', ptx_file_content)
		ptx_file_content = re.sub('\A\n+', '', ptx_file_content)
		ptx_file_content = re.sub('\A[^ -~]+', '', ptx_file_content)
		ptx_file_content = re.sub('\s+\Z', '', ptx_file_content)

		ptx_ab_name = f_name[:-8] + '_ab.txt'
		ptx_ab_path = os.path.join(generated_files_folder, dir_name, ptx_ab_name)

		# save the abstracted file
		utils.write_text_f(ptx_ab_path, ptx_file_content)