# this file contains all of the variables and regex patterns
# for processing of ptx files

import re

PTX_SUPPORTED_VERSION = '6.5'
SM_SUPPORTED_TARGET = 'sm_75'

# regex helper function - returns the list of provided
# regex patterns as one regex
def any_of(possibilities : list) -> str:
	if len(possibilities) == 0:
		error('can not form any of ... len(possibilities) == 0')

	s = '('
	for idx, pos in enumerate(possibilities):
		s += pos
		if idx == len(possibilities) - 1:
			s += ')'
		else:
			s += '|'
	return s

# #####################################
# #####	
# #####	PTX SREG identifiers
# #####
# #####################################

sreg_tid = '%tid'
sreg_ntid = '%ntid'
sreg_laneid = '%laneid'
sreg_warpid = '%warpid'
sreg_nwarpid = '%nwarpid'
sreg_ctaid = '%ctaid'
sreg_nctaid = '%nctaid'
sreg_smid = '%smid'
sreg_nsmid = '%nsmid'
sreg_gridid = '%gridid'
sreg_lmask_eq = '%lanemask_eq'
sreg_lmask_le = '%lanemask_le'
sreg_lmask_lt = '%lanemask_lt'
sreg_lmask_ge = '%lanemask_ge'
sreg_lmask_gt = '%lanemask_gt'
sreg_clk = '%clock'
sreg_clk_hi = '%clock'
sreg_clk64 = '%clock64'
sreg_tsmem_size = '%total_smem_size'
sreg_dsmem_size = '%dynamic_smem_size'

sreg_ids = [ sreg_tid, sreg_ntid, sreg_laneid, sreg_warpid,
	sreg_nwarpid, sreg_ctaid, sreg_nctaid, sreg_smid,
	sreg_nsmid, sreg_gridid, sreg_lmask_eq, sreg_lmask_le,
	sreg_lmask_lt, sreg_lmask_ge, sreg_lmask_gt, sreg_clk,
	sreg_clk_hi, sreg_clk64, sreg_tsmem_size, sreg_dsmem_size ]

# add %pm0 ... %pm7
for i in range(8):
	sreg_ids.append('%pm' + str(i))

# add %pm0_64 ... %pm7_64
for i in range(8):
	sreg_ids.append('%pm' + str(i) + '_64')

# add %envreg0 ... %envreg31
for i in range(32):
	sreg_ids.append('%envreg' + str(i))

# #####################################
# #####	
# #####	REGEX patterns
# #####
# #####################################

#####################################
####### patterns used mainly in preprocessing
#####################################

# in PTX, we have normal C-style comments
r_comments = '//.*?(?=\n)|/\*.*?\*/' 

# patterns for module directives
r_version = '\.version \d+\.\d+'
r_target = '\.target sm_(10|11|12|13|20|30|32|35|37|50|52|53|60|61|62|70|72|75)'
r_address_size = '\.address_size (32|64)'

# patterns for performance tuning directives
r_maxnreg = '\.maxnreg \d+'
r_maxntid = '\.maxntid (\d+,\s*\d+,\s*\d+|\d+,\s*\d+|\d+)'
r_reqntid = '\.reqntid (\d+,\s*\d+,\s*\d+|\d+,\s*\d+|\d+)'
r_minnctaspersm = '\.minnctapersm \d+'
r_maxnctaspersm = '\.maxnctapersm \d+'
r_noreturn = '\.noreturn'
r_pragma = '\.pragma "nounroll"\s*;'

# linking directives
# watchout - weak directive may be also placed in other contexts
r_visible = '\.visible'
r_weak = '\.weak.*?(\.func|\.entry)'
r_common = '\.common'

# removing variable initialization
r_variable_init = '[ \t]*(\.global|\.shared|\.const|\.extern).*?=\s*\{(.*)\}?;'

# for collapsing
r_collapse_kernel = '\.entry.*?\)'
r_collapse_func = '(\.func.*?)(\n\s*\{|;)'
r_collapse_call = '\\bcall.*?;'

# function matching after collapsing
r_oneline_fwd_declared_func = '^(?!\s*\.extern).*(\.func|\.entry).*;'
r_oneline_declared_func = '^\.extern.*(\.func|\.entry).*'
r_oneline_defined_func = '^(?!\.extern).*\.func.*'
r_oneline_kernel = '^(?!\.extern).*\.entry.*'

# for uncollapsing
r_uncollapse_2stmnts = '(;)(.*?)(?=;)'
r_uncollapse_t1_bracket = ';[ \t]*\{'
r_uncollapse_pt_bracket = '(})(\s*)({)'
r_uncollapse_p_bracket = '^[ \t]*(\{)(.*?;)'
r_uncollapse_t2_bracket = '(;[ \t]*)(})'

# instruction normalization
rc_norm_1 = re.compile('^([a-z][a-z0-9]*(\s*\.\s*[a-z0-9]+)*)(\s*)(.*)')
rc_norm_2 = re.compile('^(@!?[a-zA-Z][a-zA-Z0-9_$]*|@!?[_$%][a-zA-Z0-9_$]+)(\s*)(.*)')
rc_norm_3 = re.compile('^([a-z][a-z0-9]*(\s*\.\s*[a-z][a-z0-9]*)*)(\s*)(.*)')

# general variable identifier according to the ptx documentation
r_variable_identifier = '(\\b[a-zA-Z][a-zA-Z0-9_$]*|[_$%][a-zA-Z0-9_$]+)'
rc_variable_identifier = re.compile(r_variable_identifier)
# the \\b is for hexadecimal and other constants

# for label preprocessing
r_lpreprocess_label = r_variable_identifier + '\s*:.*;'
r_pred_preprocess_pred_lines = '((@!?)([a-zA-Z][a-zA-Z0-9_$]*|[_$%][a-zA-Z0-9_$]+))(.*;)'
r_pred_preprocess_branch_lines = '@.*;'
r_call_preprocess_call_lines = '\\bcall\\b.*;'

# function name identifiers
r_func_identifier = r_variable_identifier + '\s*\('
r_kernel_identifier = r_func_identifier

# patterns used for replacement of parametrized var declarations
r_parametrized_variable_declaration = r_variable_identifier + '<[0-9]+>'
r_parametrized_variable_number = '(?<=<)[0-9]+(?=>)'

# pattern for matching scope variable declarations
r_scoped_variable_declaration = '(\.reg|\.param)'

#####################################
####### patterns used mainly in XFG building
#####################################

# patterns for adding global variable nodes
r_global_variable = '(\.global|\.shared|\.const|\.extern)'

# patterns for adding function parameters into the graph
r_func_params = '\(.*?\)'

# patterns for adding local variables into the graph
r_local_variable = '(\.reg|\.param|\.shared|\.local)'

# pattern for matching label
r_label = r_variable_identifier + '\s*(?=:)'

# --- constant patterns
r_const_i_hex = '0[xX][0123456789ABCDEFabcdef]+U?'
r_const_i_oct = '0[012345678]+U?'
r_const_i_bin = '0[bB][01]+U?'
r_const_i_dec = '[123456789][0123456789]*U?'
r_const_i_dec_m = '-' + r_const_i_dec

r_const_f_fp = '0[fF][0123456789ABCDEFabcdef]{8}'
r_const_f_dp = '0[dD][0123456789ABCDEFabcdef]{16}'

# predicate constants
r_const_p_t = '1'
r_const_p_f = '0'

r_consts_i = [r_const_i_hex, r_const_i_oct,
	r_const_i_bin, r_const_i_dec_m, r_const_i_dec]

r_consts_p = [ r_const_p_t, r_const_p_f ]
rc_consts_ip = re.compile(any_of(r_consts_i + r_consts_p))

r_consts_f = [ r_const_f_fp, r_const_f_dp ]
rc_consts_f = re.compile(any_of(r_consts_f))

r_consts = [ r_const_i_hex, r_const_i_oct, r_const_i_bin,
	r_const_i_dec_m, r_const_i_dec, r_const_f_fp, r_const_f_dp,
	r_const_p_t, r_const_p_f ]

# ---- data movement and conversion patterns
# pattern for detecting vector inst
r_vector_inst = '\{.*\}'
r_inst_address_operand = '(?<=\[).*?(?=\])'

r_dmc_mov = 'mov'
r_dmc_shfl = 'shfl'
r_dmc_prmt = 'prmt'
r_dmc_ld = 'ld\\b'
r_dmc_ldu = 'ldu'
r_dmc_st = 'st'
r_dmc_cvta = 'cvta'
r_dmc_cvt = 'cvt\\b'
r_dmc_isspacep = 'isspacep'

r_dmc_insts = [ r_dmc_mov, r_dmc_shfl, 
	r_dmc_prmt, r_dmc_ld, r_dmc_ldu,
	r_dmc_st, r_dmc_cvta, r_dmc_cvt,
	r_dmc_isspacep]

rc_dmc_all = re.compile(any_of(r_dmc_insts))

# ---- arithmetic instructions patterns
r_a_add = 'add\\b'
r_a_sub = 'sub\\b'
r_a_mul = 'mul\\b'
r_a_mad = 'mad\\b'
r_a_div = 'div'
r_a_abs = 'abs'
r_a_neg = 'neg'
r_a_min = 'min'
r_a_max = 'max'

r_ia_mul24 	= 'mul24'
r_ia_mad24 	= 'mad24'
r_ia_sad 	= 'sad'
r_ia_rem 	= 'rem'
r_ia_popc 	= 'popc'
r_ia_clz 	= 'clz'
r_ia_bfind 	= 'bfind'
r_ia_fns 	= 'fns'
r_ia_brev 	= 'brev'
r_ia_bfe 	= 'bfe'
r_ia_bfi 	= 'bfi'
r_ia_dp4a 	= 'dp4a'
r_ia_dp2a 	= 'dp2a'
r_ia_addc 	= 'addc'
r_ia_subc 	= 'subc'
r_ia_madc	= 'madc'

r_fpa_testp = 'testp'
r_fpa_copysign = 'copysign'
r_fpa_fma = 'fma'
r_fpa_rcp = 'rcp'
r_fpa_sqrt = 'sqrt'
r_fpa_rsqrt = 'rsqrt'
r_fpa_sin = 'sin'
r_fpa_cos = 'cos'
r_fpa_lg2 = 'lg2'
r_fpa_ex2 = 'ex2'

r_a_cmn = [ r_a_add, r_a_sub, r_a_mul,
	r_a_mad, r_a_div, r_a_abs, r_a_neg, r_a_min, r_a_max]

r_ia_all = [ r_ia_mul24, r_ia_mad24,
	r_ia_sad, r_ia_rem, r_ia_popc, r_ia_clz, r_ia_bfind,
	r_ia_fns, r_ia_brev, r_ia_bfe, r_ia_bfi, r_ia_dp4a,
	r_ia_dp2a, r_ia_addc, r_ia_subc, r_ia_madc]

r_fpa_all = [ r_fpa_testp, r_fpa_copysign,
	r_fpa_fma, r_fpa_rcp, r_fpa_sqrt, r_fpa_rsqrt, r_fpa_sin,
	r_fpa_cos, r_fpa_lg2, r_fpa_ex2]

rc_a_all = re.compile(any_of(r_a_cmn + r_ia_all + r_fpa_all))

# ---- comparison and selection instruction patterns
r_cmpsel_set = 'set\\b'
r_cmpsel_setp = 'setp'
r_cmpsel_selp = 'selp'
r_cmpsel_slct = 'slct'

r_cmpsel_insts = [ r_cmpsel_set, r_cmpsel_setp,
	r_cmpsel_selp, r_cmpsel_slct ]

rc_cmpsel_all = re.compile(any_of(r_cmpsel_insts))

# ---- logic and shift instruction patterns
r_ls_and = 'and'
r_ls_or  = 'or'
r_ls_xor = 'xor'
r_ls_not = 'not'
r_ls_cnot = 'cnot'
r_ls_lop3 = 'lop3'
r_ls_shf = 'shf'
r_ls_shl = 'shl'
r_ls_shr = 'shr'

r_ls_insts = [ r_ls_and, r_ls_or, r_ls_xor, r_ls_not,
	r_ls_cnot, r_ls_lop3, r_ls_shf, r_ls_shl, r_ls_shr ]

rc_ls_all = re.compile(any_of(r_ls_insts))

# ---- control flow instruction patterns
r_cf_bra = 'bra'
r_cf_brx = 'brx'
r_cf_pred = '@'
r_cf_ret = 'ret'
r_cf_exit = 'exit'
r_cf_call = 'call'

r_cf_insts = [ r_cf_pred, r_cf_bra, r_cf_brx, 
	r_cf_ret, r_cf_exit, r_cf_call ]

rc_cf_all = re.compile(any_of(r_cf_insts))

# ---- parallel synchronization and communication patterns
r_psc_bar = 'bar'
r_psc_membar = 'membar'
r_psc_fence = 'fence'
r_psc_atom = 'atom'
r_psc_red = 'red'
r_psc_vote = 'vote'
r_psc_match = 'match'
r_psc_amask = 'activemask'

r_psc_insts = [r_psc_bar, r_psc_membar,
	r_psc_fence, r_psc_atom, r_psc_red,
	r_psc_vote, r_psc_match, r_psc_amask]

rc_psc_all = re.compile(any_of(r_psc_insts))

# ---- texture memory instruction patterns
r_tex_tex = 'tex'
r_tex_tld4 = 'tld4'
r_tex_txq = 'txq'
r_tex_istypep = 'istypep'

r_tex_insts = [ r_tex_tex, r_tex_tld4, r_tex_txq,
	r_tex_istypep ]

rc_tex_all = re.compile(any_of(r_tex_insts))

# ---- surface memory instruction patterns
r_surf_suld = 'suld'
r_surf_sust = 'sust'
r_surf_sured = 'sured'
r_surf_suq = 'suq'

r_surf_insts = [ r_surf_suld, r_surf_sust,
	r_surf_sured, r_surf_suq ]

rc_surf_all = re.compile(any_of(r_surf_insts))

# ---- warp-level MMA instruction patterns
r_wmma_load = 'wmma\.load'
r_wmma_store = 'wmma\.store'
r_wmma_mma = 'wmma\.mma'
r_mma_mma = 'mma'
r_mma_ldmatrix = 'ldmatrix'

r_wmma_insts = [ r_wmma_load, r_wmma_store,
	r_wmma_mma, r_mma_mma, r_mma_ldmatrix ]

rc_wmma_all = re.compile(any_of(r_wmma_insts))

# ---- misc instruction patterns
r_misc_trap = 'trap'

r_misc_insts = [ r_misc_trap ]

# ---- video instruction patterns
r_vid_vadd = 'vadd\\b'
r_vid_vadd2 = 'vadd2'
r_vid_vadd4 = 'vadd4'
r_vid_vsub = 'vsub\\b'
r_vid_vsub2 = 'vsub2'
r_vid_vsub4 = 'vsub4'
r_vid_vmad = 'vmad'
r_vid_vargv2 = 'vargv2'
r_vid_vargv4 = 'vargv4'
r_vid_vabsdiff = 'vabsdiff\\b'
r_vid_vabsdiff2 = 'vabsdiff2'
r_vid_vabsdiff4 = 'vabsdiff4'
r_vid_vmin = 'vmin\\b'
r_vid_vmin2 = 'vmin2'
r_vid_vmin4 = 'vmin4'
r_vid_vmax = 'vmax\\b'
r_vid_vmax2 = 'vmax2'
r_vid_vmax4 = 'vmax4'
r_vid_vshl = 'vshl'
r_vid_vshr = 'vshr'
r_vid_vset = 'vset\\b'
r_vid_vset2 = 'vset2'
r_vid_vset4 = 'vset4'

r_vid_vec1_insts = [ r_vid_vadd, r_vid_vsub, r_vid_vabsdiff,
	r_vid_vmin, r_vid_vmax, r_vid_vshl, r_vid_vshr,
	r_vid_vmad, r_vid_vset]

r_vid_vec2_insts = [ r_vid_vadd2, r_vid_vsub2, r_vid_vargv2,
	r_vid_vabsdiff2, r_vid_vmin2, r_vid_vmax2, r_vid_vset2 ]

r_vid_vec4_insts = [ r_vid_vadd4, r_vid_vsub4, r_vid_vargv4,
	r_vid_vabsdiff4, r_vid_vmin4, r_vid_vmax4, r_vid_vset4 ]

rc_vid_all = re.compile(any_of(r_vid_vec1_insts + r_vid_vec2_insts + r_vid_vec4_insts))

# ---- special patterns related to video instructions that 
# are used in preprocessing phase ... with these operands,
# we remove all of the operand qualifiers

# general operand qualifiers that may be used with any vector register
r_vec_idxs = [ '\.x\\b', '\.y\\b', '\.z\\b',
	'\.w\\b', '\.r\\b', '\.g\\b', '\.b\\b', '\.a\\b' ]
rc_vec_idxs = re.compile(any_of(r_vec_idxs))

# operand qualifiers used only with vector instructions
r_vid_vec1_sc = []
r_vid_vec2_sc = []
r_vid_vec4_sc = []

for i in r_vid_vec1_insts:
	r_vid_vec1_sc.append(i + '.*;')

for i in r_vid_vec2_insts:
	r_vid_vec2_sc.append(i + '.*;')

for i in r_vid_vec4_insts:
	r_vid_vec4_sc.append(i + '.*;')

rc_vid_all_sc = re.compile(any_of(r_vid_vec1_sc + r_vid_vec2_sc + r_vid_vec4_sc))

rc_vec_qs = re.compile('(\.b[0-9]{1,4}\\b|\.h[0-9]{1,4}\\b)')





