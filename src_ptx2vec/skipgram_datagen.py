# This file implements the data generation phase of ptx2vec
#
# This phase takes all of the dual XFG graphs produced by the XFG transformation
# phase and the vocabulary that was produced by the vocabulary building phase
# and it generates training data pairs for the skip-gram model.
#
# There are two important parameters to this phase -- subsampling threshold and
# context size. Subsapling is a technique used in the generation of data for the
# Skip-Gram model that limits the occurence of too frequent data pairs. Parameter
# context-size sets the size of a context window for each instruction.

import os
import re
import json
import math
import random

import pickle
import networkx as nx
import struct

from dataclasses import dataclass

from src_common import utils

@dataclass
class emb_train_data:
	cutoff : int
	context_size : int
	subsampling_threshold: int
	training_pairs : list

def add_dual_to_vocab_pairs(D, vocab_idx, vocab_coff, unk_stmt_idx, context_size):
	# this function finds the context of each instruction and adds this context
	# to a vocabulary where keys are tuples of instructions (acutally, tuples of indices
	# to the vocabulary) and values are the number of occurences of this particular
	# contextual pair
	vocab_idx_pairs = {}
	A_orig = nx.adjacency_matrix(D)

	A_context = A_orig
	A_tmp = A_orig

	for i in range(1, context_size):
		A_tmp *= A_orig
		A_context += A_tmp

	# now, matrix A_context has nonzero value at idx [i][j] if node
	# j is reachable from i within context_size steps

	del A_tmp
	del A_orig

	# note that the matrices are in CSR sparse format
	A_row_count = A_context.shape[0]
	A_indices = A_context.indices
	A_row_starts = A_context.indptr

	nodelist = list(D.nodes())
	for i in range(A_row_count):
		col_indices = A_indices[A_row_starts[i]:A_row_starts[i + 1]]

		for j in col_indices:
			
			if i != j:
				src = re.sub('§\d+$', '', nodelist[i])
				dest_context = re.sub('§\d+$', '', nodelist[j])

				if src != dest_context:
					if src not in vocab_idx and src not in vocab_coff:
						utils.error('can not form context pair for src: ' + str(src) + ' ... node is not in vocab_idx')

					if dest_context not in vocab_idx and dest_context not in vocab_coff:
						utils.error('can not form context pair for dest: ' + str(dest_context) + ' ... node is not in vocab_idx')

					if src in vocab_idx:
						s_idx = vocab_idx[src]
					else:
						s_idx = unk_stmt_idx

					if dest_context in vocab_idx:
						d_idx = vocab_idx[dest_context]
					else:
						d_idx = unk_stmt_idx

					if s_idx == unk_stmt_idx and d_idx == unk_stmt_idx:
						continue

					if (s_idx, d_idx) in vocab_idx_pairs:
						vocab_idx_pairs[(s_idx, d_idx)] += 1
					else:
						vocab_idx_pairs[(s_idx, d_idx)] = 1


	return vocab_idx_pairs

def generate_training_pairs(vocab_idx_pairs, subs_t):
	# this function applies subsampling to the vocabulary of contextual
	# pairs
	training_pairs = []

	context_pairs_count = sum(count for pair, count in vocab_idx_pairs.items())
	
	for pair, count in vocab_idx_pairs.items():
		idx1, idx2 = pair

		# discard probability
		p_discard = max(1.0 - math.sqrt(subs_t / (count / context_pairs_count)), 0.0)

		for i in range(count):
			if random.random() > p_discard:
				training_pairs.append([idx1, idx2])
			if random.random() > p_discard:
				training_pairs.append([idx2, idx1])

	return training_pairs

#####################################
####### PUBLIC API
#####################################
def generate_training_data(generated_files_folder, restore, context_size, subsampling_threshold):
	# this is the main function of the training data generation phase
	#
	# as already stated, the training pairs for the skip-gram model are generated
	# based on the dual xfgs produced by the xfg transformation phase and the vocabulary
	# produced by the vocabulary building phase
	#
	# it goes over each dual XFG and for each dual XFG, it builds an adjacency matrix;
	# this adjacency matrix can now be used to easily determine the context of each
	# instruction; based on this context, first, we create a vocabulary of all the
	# contextual pairs along with their number of occurences in the graph and then
	# we apply subsampling to these contextual pairs ... the contextual pairs that
	# are not discarded in the subsampling process are then appended to the list
	# of the actual training instruction pairs

	utils.report_phase_started('embedding training data generation')
	
	# first, we need to load the vocabulary with indices
	vocab_file_path = os.path.join(generated_files_folder, 'vocab_idx.json')
	vocab_coff_path = os.path.join(generated_files_folder, 'vocab_coff.json')
	emb_train_pairs_path = os.path.join(generated_files_folder, 'emb_train_data.p')

	# check if the generation has already been performed
	if restore and os.path.exists(emb_train_pairs_path):
		return

	# load idx vocabulary
	if not os.path.exists(vocab_file_path):
		utils.error('can not generate training data: missing vocab_idx file')

	vocab_idx = utils.read_json(vocab_file_path)
	coff = vocab_idx['cutoff']
	unk_stmt_idx = vocab_idx['unk_stmt_idx']
	vocab_idx = vocab_idx['vocab_idx']

	# load cut off vocabulary if present
	if not os.path.exists(vocab_coff_path):
		vocab_coff = {}
	else:
		vocab_coff = utils.read_json(vocab_coff_path)

	training_pairs = []
	# now go over each dual XFG
	for _, _, D in utils.read_files(generated_files_folder,
		'xfg_dual.p', utils.pickle_generic_r):
		
		# create its contextual pairs
		vocab_idx_pairs = add_dual_to_vocab_pairs(D, vocab_idx, vocab_coff, unk_stmt_idx, context_size)
		# apply subsampling and add them to the list of actual training data pairs
		training_pairs += generate_training_pairs(vocab_idx_pairs, subsampling_threshold)

	train_data = emb_train_data(coff, context_size, subsampling_threshold, training_pairs)

	# pickle the training data pairs for the training phase 
	# and training with the Skip-Gram model
	utils.pickle_generic_w(emb_train_pairs_path, train_data)
			
