# This file implements the XFG transformation phase that transforms
# each abstract XFG graph that is produced by the XFG abstraction
# phase into a dual graph (formally called line graph)
# 
# note that this transformation involves one additional step;
# in order to make the edges of original XFG lines in the dual XFG,
# we need to disambiguate each edge (we do this by appending §<counter>),
# because each node in the dual xfg needs a unique identifier and the original
# XFG contains duplicate instructions

import os
import pickle
import networkx as nx

from src_common import utils
from src_ptx2vec import xfg_graph

#####################################
####### EDGE DISAMBIGUATION
#####################################

def disambiguate_edges(xfg):
	# disambiguate the edges (instructions) of the original XFG by appending
	# §<counter> to each instruction
	dic = {}
	
	for v1, v2, edge_id, label in xfg.edges_it():
		counter = 0
		if label in dic:
			dic[label] += 1
			counter = dic[label] 
		else:
			dic[label] = 0
			
		xfg.set_edge_label(v1, v2, edge_id, label + '§' + str(counter))

#####################################
####### XFG TO DUAL
#####################################

def get_edges_to_explore(node, xfg, explored):
	out_neighbours = list(xfg.G().successors(node))

	# filter already visited neighbours
	out_neighbours = [n for n in out_neighbours if not n in explored]

	to_explore_edges = []	
	for neighbour in out_neighbours:
		edge_labels = xfg.get_all_edge_labels(node, neighbour)

		for edge_label in edge_labels:
			# do not add already visited edges
			if edge_label in explored:
				continue
			to_explore_edges.append((neighbour, edge_label))

	return to_explore_edges

def get_actual_neighbour_labels(edge, xfg):
	v1, v2, label = edge

	# getting actual neighbour labels involves skipping through
	# the empty edges that are present in the graph ... this 
	# transform the problem into a sort of bfs, where we explore
	# the graph until each path does not end with non-empty edge
	explored = set()
	explored.add(label)
	
	actual_neighbouring_labels = []

	# node queue to explore
	to_explore_q = [v2]

	while len(to_explore_q) > 0:
		to_explore_n = to_explore_q.pop(0)

		# this returns non-visited edges along with the neighbouring
		# node (of to_explore_n)
		to_explore_edges = get_edges_to_explore(to_explore_n, xfg, explored)

		for neighbour, neighbour_label in to_explore_edges:
			# if the edge is not empty, then good, this is the actual next
			# neighbouring statement ... otherwise, we need to explore
			# the neighbouring node
			if xfg_graph.XFG.is_empty_edge(neighbour_label):
				to_explore_q.append(neighbour)
			else:
				actual_neighbouring_labels.append(neighbour_label)

			explored.add(neighbour_label)

		explored.add(to_explore_n)

	return actual_neighbouring_labels


def build_xfg_dual(xfg):
	# in order to transform XFG into dual graph, we need to go through
	# each edge in the original XFG, add it into the dual graph and
	# find its neighbouring labels

	# intialization of dual graph
	D = nx.Graph()

	for v1, v2, edge_id, label in xfg.edges_it():
		# skip through ctrl edges (these are empty edges)
		if xfg_graph.XFG.is_empty_edge(label):
			continue

		# add new node for this label if not already present
		if not label in D.nodes():
			D.add_node(label)

		# get actual neighbouring edge labels
		actual_neighbour_labels = get_actual_neighbour_labels((v1, v2, label), xfg)

		# and add edges in the dual graph
		for n_label in actual_neighbour_labels:
			if not n_label in D.nodes():
				D.add_node(n_label)
			if not D.has_edge(label, n_label):
				D.add_edge(label, n_label)

	return D


#####################################
####### PUBLIC API
#####################################
def transform_xfgs(generated_files_folder, restore):
	# this is the main function that performs the XFG transformation phase
	# 
	# it goes over each abstract XFG graph that was produced by the XFG
	# abstraction phase and it transform each of these graphs into a dual XFG

	utils.report_phase_started('transforming xfgs')

	for dir_name, f_name, xfg_tuple in utils.read_files(generated_files_folder,
		'_xfg_ab.p', utils.pickle_xfg_r):
		_, _, xfg = xfg_tuple

		xfg_d_name = f_name[:-5] + '_dual.p'
		xfg_d_fp = os.path.join(generated_files_folder, dir_name, xfg_d_name)

		if restore and os.path.exists(xfg_d_fp):
			continue

		# first, disambiguate the edges
		disambiguate_edges(xfg)

		# create the dual graph
		D = build_xfg_dual(xfg)

		# and save it for the follwing phases
		utils.pickle_generic_w(xfg_d_fp, D)
