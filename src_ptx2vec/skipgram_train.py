# This file implements the training phase of ptx2vec.
#
# This phase takes the training data produced by the training data generation
# phase and vocabulary produced by the vocabulary building phase and it trains
# the ptx2vec embedding space with the Skip-Gram model. 

import os
import tensorflow as tf
import math
import numpy as np
from dataclasses import dataclass
from shutil import copyfile

from src_common import utils

@dataclass
class emb_params:
	batch_size : int
	epochs : int
	embedding_dim : int
	epoch_report_freq : int

#####################################
####### SKIP-GRAM TRAINING
#####################################
def train_skip_gram(train_pairs, vocab_idx, skip_gram_params):
	train_X = [p[0] for p in train_pairs]
	train_Y = [p[1] for p in train_pairs]

	ds_size = len(train_X)
	ds = tf.data.Dataset.from_tensor_slices((train_X, train_Y))
	ds = ds.shuffle(int(1e5))
	ds = ds.batch(skip_gram_params.batch_size, drop_remainder=True)

	it = ds.make_initializable_iterator()	
	train_X_t, train_Y_t = it.get_next()

	X_one_hot = tf.one_hot(train_X_t, len(vocab_idx))
	Y_one_hot = tf.one_hot(train_Y_t, len(vocab_idx))

	embedding_layer = tf.layers.dense(X_one_hot, units=skip_gram_params.embedding_dim, activation=None, use_bias=False)
	Y_PRED = tf.layers.dense(embedding_layer, units=len(vocab_idx), activation=tf.nn.softmax)

	loss = tf.losses.softmax_cross_entropy(onehot_labels=Y_one_hot, logits=Y_PRED)
	optimizer = tf.train.AdamOptimizer().minimize(loss)

	init = tf.global_variables_initializer()

	with tf.Session() as sess:
		# initialize variables of the whole network
		sess.run(init)

		epoch_counter = 0
		epoch_steps = math.floor(ds_size / skip_gram_params.batch_size)
		report_steps = max(1, math.floor(epoch_steps / skip_gram_params.epoch_report_freq))

		while epoch_counter < skip_gram_params.epochs:
			# first - initialize iterator again with the dataset
			sess.run(it.initializer)

			# initialize counters
			epoch_step_counter = 0
			epoch_acc_loss = 0

			utils.report_epoch_started(epoch_counter, skip_gram_params.epochs)
			while True:
				try:
					_, batch_loss = sess.run((optimizer, loss))

					epoch_acc_loss += batch_loss
					epoch_step_counter += 1
					if epoch_step_counter % report_steps == 0:
						utils.report_batch_loss(epoch_step_counter, batch_loss)

				except tf.errors.OutOfRangeError:
					epoch_counter += 1

					utils.report_epoch_ended(epoch_acc_loss / epoch_step_counter)
					break

		emb_matrix = tf.get_default_graph().get_tensor_by_name(os.path.split(embedding_layer.name)[0] + '/kernel:0').eval()
		return emb_matrix


#####################################
####### OUTPUT RELATED
#####################################

def copy_emb_related_files(generated_files_folder, emb_folder_path):
	# this function copies some essential files to the output folder 
	# next to the embedding matrix
	copyfile(os.path.join(generated_files_folder, 'vocab_idx.json'), os.path.join(emb_folder_path, 'vocab_idx.json'))
	copyfile(os.path.join(generated_files_folder, 'vocab_freq.json'), os.path.join(emb_folder_path, 'vocab_freq.json'))

	if os.path.exists(os.path.join(generated_files_folder, 'vocab_coff.json')):
		copyfile(os.path.join(generated_files_folder, 'vocab_coff.json'), os.path.join(emb_folder_path, 'vocab_coff.json'))

def normalize_emb_matrix(emb_matrix):
	# this function normalizes the embedding matrix
	for i in range(len(emb_matrix)):
		emb_matrix[i] = emb_matrix[i] / np.linalg.norm(emb_matrix[i])
	
	return emb_matrix

def report_emb_stats(emb_folder_path, cutoff, context_size,
 	subs_threshold, train_pairs, skip_gram_params, ab_granularity):
	# this function outputs some of the settings used in the training 
	# of ptx2vec embedding space
	stats = {
		'cutoff': cutoff,
		'context_size': context_size,
		'subs_threshold': subs_threshold,
		'train_pairs_count': len(train_pairs),
		'batch_size': skip_gram_params.batch_size,
		'epochs': skip_gram_params.epochs,
		'emb_dim': skip_gram_params.embedding_dim,
		'ab_granularity': ab_granularity
	}

	stats_fp = 	os.path.join(emb_folder_path, 'emb_stats.json')
	utils.write_json(stats_fp, stats)

#####################################
####### PUBLIC API
#####################################
def train_embeddings(generated_files_folder, out_folder, skip_gram_params):
	# this is the main public function of the training phase;
	#
	# it loads the necessary files for the training -- training data and vocabulary
	# and then delegates the control to the train_skip_gram function that
	# implements the skip-gram model
	# 
	# train_skip_gram function returns the embedding matrix that 
	# can be saved and, along with the vocabulary, it can be used for various GPU
	# optimization tasks

	utils.report_phase_started('embedding training')

	vocab_idx_path = os.path.join(generated_files_folder, 'vocab_idx.json')
	emb_train_data_path = os.path.join(generated_files_folder, 'emb_train_data.p')

	if not os.path.exists(vocab_idx_path) or not os.path.exists(emb_train_data_path):
		utils.error('can not train embeddings: missing either vocab_idx or emb_train_data')

	# first, load the necessary files
	vocab_idx = utils.read_json(vocab_idx_path)
	ab_granularity = vocab_idx['ab_granularity']
	vocab_idx = vocab_idx['vocab_idx']
	train_data = utils.pickle_generic_r(emb_train_data_path)

	# extract additional information from the training data
	cutoff = train_data.cutoff
	context_size = train_data.context_size
	subs_threshold = train_data.subsampling_threshold
	train_pairs = train_data.training_pairs

	# perform the training
	emb_matrix = train_skip_gram(train_pairs, vocab_idx, skip_gram_params)
	# normalize the embedding matrix
	emb_matrix = normalize_emb_matrix(emb_matrix)

	# output
	# detect the number of folders in the out folder (the resulting
	# folder will then be "emb_<len>")
	emb_num = len([i for i in os.listdir(os.path.join(out_folder, '')) if os.path.isdir(os.path.join(out_folder, i))])
	emb_folder_name = f'emb_{emb_num}'
	emb_folder_path = os.path.join(out_folder, emb_folder_name)
	os.mkdir(emb_folder_path)

	copy_emb_related_files(generated_files_folder, emb_folder_path)

	report_emb_stats(emb_folder_path, cutoff, context_size, subs_threshold,
		 train_pairs, skip_gram_params, ab_granularity)

	# finally, save the embedding matrix
	utils.pickle_generic_w(os.path.join(emb_folder_path, 'emb_matrix.p'), emb_matrix)

	
