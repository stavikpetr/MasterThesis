## Overview
This folder contains the vast majority of code that is related to the implementation of our ptx2vec approach. The whole process of training of ptx2vec embedding space is executed from the top-level file *.\./train_ptx2vec.py*, while this folder contains the implementation of core modules that e.g. build the XFG graphs or they perform the training. Because the implementation itself is quite complicated, let us briefly describe some of its most important parts. 

## Implementation
The implementation of ptx2vec essentially consists of the following phases:
- Preprocessing phase - transforms all of the input PTX files into a normalized format simplifies the implementation of the subsequent phases
- XFG building phase - builds the XFGs out of the preprocessed PTX files
- XFG abstraction phase - converts each PTX instruction in the graphs into its abstract form
- XFG transformation phase - transforms XFG into dual graphs
- Vocabulary building phase - builds the vocabulary of PTX instructions
- Training data generation phase - generates training data pairs for the Skip-Gram model
- Training phase - performs the training of the Skip-Gram model

All of these phases are invoked from the top-level *.\./train_ptx2vec.py* file. This is the main file of the whole ptx2vec approach and this is the only point in the code where each phase is invoked. This means that the phases do not call each other or communicate in any other way. Instead, each phase takes as a parameter some folder with data prepared by one of the previous phases. The respective phase then reads these data and generates new data for the next phase (or phases).

In addition, there are three more folders that are used in the implementation. Folder *.\./ptx2vec_in* contains all of the input files for the ptx2vec approach. This folder is used only in the preprocessing phase. Most of the remaining phases work with the *.\./ptx2vec_gen_files* folder. This folder will contain all of the files that will be generated by the phases. Lastly, there is the folder *.\./ptx2vec_out*. This folder is used only by the last phase and this folder will contain the resulting embedding matrix and vocabulary that was trained with the Skip-Gram model and that can be used in various GPU optimization tasks.

### Preprocessing phase
Preprocessing phase is implemented in the file *preprocesser.py*. This phase goes over each PTX file in the input folder (by default, this is folder *.\./ptx2vec_in*), preprocesses each file, and outputs each preprocessed file into the generated files folder. The implementation of this phase consists of many transformations that are performed with the use of regular expressions. Many of the used regular expressions are defined in *ptx_info.py* file. All of the transformations performed by this phase are extensively described in the aforementioned preprocesser file.

### XFG building phase
XFG building phase is implemented in two files. First, there is file *xfg_builder.py* which performs the building of XFG graph. The second file is *xfg_graph.py*, which contains the actual implementation of the graph itself.

The XFGs are built from the preprocessed files that were generated by the preprocessing phase. We do this by iterating over each instruction and adding it to the graph. However, the actual algorithm for building the graphs is quite complicated and it would not be possible to describe it here. Please, refer to the description of the algorithm for building XFG graphs in the thesis. These steps should be fairly easily mapped into the respective functions in the two aforementioned files that contain additional comments for many of the XFG building cases.

### XFG abstraction phase
XFG abstraction phase is implemented in *xfg_abstracter.py* file. This phase transforms each instruction in the XFG graphs produced by the previous phases into their abstract form. This phase is fairly straight-forward. It simply iterates over the edges and performs abstraction as was mentioned in the thesis. 

However, there are few more things that are worth mentioning -- first, the correct mapping of operands to the abstract elements like <REG> or <LBL> has one additional prerequisite. When we iterate over the instructions, we can not be certain which operand is what (nvcc compiler follows a few conventions, however, the programmer can use anything in inline assembler). This means that before we can perform the abstraction, we need to know the type of each operand (meaning register vs label). We do this by collecting the context of each function and the context of the whole PTX file. By context we mean the lists of all variables, registers, parameters and so on. The definition of contexts can be found in file *xfg_context.py*. The contexts are actually collected in the previous, XFG building phase and they are saved for the XFG abstraction phase.

Second -- this phase outputs two types of files. First, it outputs the graphs where each instruction is in its abstract form. This is the expected output. However, it also essentially outputs the list of these edges. This list of edges will be used in the vocabulary building phase for building of the vocabulary (we could also use the graphs, however, this is much more convenient).

Lastly -- this file contains two implementations of abstraction functions. First, there is the very fine abstraction that is used in ptx2vec. However, this file also contains the implementation of the coarse abstraction function of the ptx_ab approach. 

### XFG transformation phase
This phase is implemented in *xfg_transformer.py*. It takes all of the abstract XFGs and transforms each XFG into a dual graph. This is performed mainly for convenience because it allows to easily determine the context of each instruction. 

This phase is fairly simple, it goes over each edge in the original XFG, detects neighbouring edges and adds them to the dual XFG. One thing worth mentioning is that there is one additional step involved, which is disambiguation of instructions. The problem is, that the edges of the original XFG contain labels that are not unique. This means that we would not be able to easily identify the nodes in the dual graph. Therefore, each edge is disambiguated by appending �<counter> to each instruction.

### Vocabulary building phase
This phase is implemented in *voc_builder.py*. It takes the files with abstract instructions produced by the XFG abstraction phase and it forms a vocabulary out of them. In addition, it also applies cutoff. This way, each instruction that appears less than cutoff times is discarded from the vocabulary.

### Training data generation phase
This phase is implemented in *skipgram_datagen.py* and it generates the training data for the Skip-Gram model. First it determines the context of each instruction in the XFG dual graph by using adjacency matrices. After the context is determined, it applies subsampling to the contextual pairs. Pairs that are not discarded by subsampling then form the training dataset. 

### Training phase
The last training phase that is implemented in *skipgram_train.py* implements the Skip-Gram model and performs the training. After the training, it outputs the embedding matrix and vocabulary for uses in future optimization tasks.

