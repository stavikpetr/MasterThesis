# This is the top level file for the whole ptx2vec approach.
#
# It can be launched in two basic modes. In gen_train_data mode, it
# generates the training data pairs and vocabulary for the Skip-Gram model
# by gradually invoking the following phases: preprocessing phase,
# XFG building phase, XFG abstraction phase, XFG transformation phase,
# vocabulary building phase and training data generation phase.
#
# In train_skipgram mode, it invokes the final, training phase
# that uses the generated training data and vocabulary and trains
# the ptx2vec embedding space with the Skip-Gram model.
#
# It works over three basic folders: ptx2vec_in_folder contains
# PTX files that form the whole training dataset for ptx2vec,
# ptx2vec_gen_files_folder is used in each files for storing of
# various generated files (xfgs, vocabulary, training data pairs)
# lastly, ptx2vec_out contains the resulting embedding matrix and 
# vocabulary that can be used in subsequent optimization tasks.

import os
import shutil
from absl import flags, app

from src_common import appflags
from src_common import utils
from src_ptx2vec import preprocesser
from src_ptx2vec import xfg_builder
from src_ptx2vec import xfg_abstracter
from src_ptx2vec import xfg_transformer
from src_ptx2vec import voc_builder
from src_ptx2vec import skipgram_datagen
from src_ptx2vec import skipgram_train
from src_ptx2vec.skipgram_train import emb_params

FLAGS=flags.FLAGS

def main(argv):
	# for arguments, we are using the absl library, the original flags are ommited
	del argv

	ptx2vec_in_folder = FLAGS.ptx2vec_in_folder
	ptx2vec_gen_files_folder = FLAGS.ptx2vec_gen_files_folder
	ptx2vec_out_folder = FLAGS.ptx2vec_out_folder

	restore = FLAGS.ptx2vec_restore
	mode = FLAGS.ptx2vec_mode

	utils.report_program_start(mode)
	utils.report_restore_flag(restore)

	if mode == 'gen_train_data':		
		preprocesser.preprocess(ptx2vec_in_folder, ptx2vec_gen_files_folder, restore)
		xfg_builder.build_xfgs(ptx2vec_gen_files_folder, restore)

		ab_granularity = FLAGS.ptx2vec_ab_granularity
		xfg_abstracter.abstract_xfgs(ptx2vec_gen_files_folder, ab_granularity, restore)
		xfg_transformer.transform_xfgs(ptx2vec_gen_files_folder, restore)

		cutoff = FLAGS.ptx2vec_cutoff		
		context_size = FLAGS.ptx2vec_context_size
		subsampling_threshold = FLAGS.ptx2vec_subsampling_threshold

		voc_builder.build_vocabulary(ptx2vec_gen_files_folder, restore, cutoff, ab_granularity)
		skipgram_datagen.generate_training_data(ptx2vec_gen_files_folder, restore, context_size, subsampling_threshold)
	elif mode == 'train_skipgram':
		batch_size = FLAGS.ptx2vec_batch_size
		epochs = FLAGS.ptx2vec_epochs
		embedding_dim = FLAGS.ptx2vec_embedding_dim
		epoch_report_freq = FLAGS.ptx2vec_epoch_report_freq
		skip_gram_params = emb_params(batch_size, epochs, embedding_dim, epoch_report_freq)
		
		skipgram_train.train_embeddings(ptx2vec_gen_files_folder, ptx2vec_out_folder, skip_gram_params)
	

if __name__ == '__main__':
	app.run(main)