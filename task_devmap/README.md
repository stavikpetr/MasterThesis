Base folder for heterogeneous device mapping task.

Structure:

- in - folder with input kernels
- gen_files - folder that will contain various files that were generated during the training and evaluation of this task
- out - folder that will contain the training results
- cgo17-amd.csv - file with measuremenets for platform with AMD gpu
- cgo17-nvidia.csv - file with measurements for platform with NVIDIA GPU