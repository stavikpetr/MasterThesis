# This script generates all of the ptx source files for
# training of embeddings. To do this, we use the
# data provided by PublicGitArchive https://github.com/src-d/datasets/tree/master/PublicGitArchive.
# Basically, this script goes through repositories
# that are listed as containing CUDA code (by PGA),
# it clones them and then tries to compile each of the
# present .cu file. The resulting ptx files will 
# be present in $dataset_dir directory.

# By default, this script looks to the $dataset_dir
# and clones and compiles the respective repository
# only if it is not already compiled. This can be changed
# by the $clean_up bool variable. With this variable set
# the script first removes all of the generated files
# and tries to compile each repository again

dataset_dir="./"
dataset_tmp_dir="dataset_ptx_tmp"

# file with all of the repositories that contain CUDA code
cuda_repos_file="cuda_repos_all.txt"
processed_repos_file="processed_repos"
err_file="err_file"

# limit for discarding .ptx files with not enough lines
line_count_limit=50

# this bool controls whether or not are the compilation
# error files kept
keep_err_files=0

# whether or not to clean up
clean_up=0

if [ ! -f "$cuda_repos_file" ]; then
    echo "there is no file with cuda repositories - exitting ..."
    exit
fi

if [ "$clean_up" -eq 1 ]; then
    echo "cleaning up ..."
    rm -rf $dataset_dir
    rm $processed_repos_file
fi

if [ ! -d "$dataset_dir" ]; then
    mkdir $dataset_dir
fi

if [ ! -f "$processed_repos_file" ]; then
    touch $processed_repos_file
fi

rm -rf $dataset_tmp_dir
mkdir $dataset_tmp_dir

echo "processing cuda repositories one by one ..."

repository_count=`cat $cuda_repos_file | wc -l`
repository_counter=0

cd $dataset_tmp_dir

ds_files_total=0
ds_files_compiled=0
ds_files_included=0

while read -r cuda_repo_line; do
    repository_counter=$((repository_counter + 1))
    echo -e "\t---- processing repository: $cuda_repo_line ($repository_counter out of $repository_count) ----"
    
    #first, let's check that this repository was not already processed
    grep -Fxq "$cuda_repo_line" "../$processed_repos_file"

    if [ $? -eq 0 ]; then
        echo -e "\t\tthis repository is already processed ... continuing"
        continue
    fi

    echo -en "\t\tcloning ... "

    git clone $cuda_repo_line &> /dev/null

    if [ $? -eq 0 ]; then
        echo "DONE"
    else
        echo "FAILED ... continuing"
        continue
    fi

    #first, let's delete all ptx files that may be already present
    find . -name "*.ptx" -type f -delete
    
    #extracting repository name from the url
    repo_name=$(basename -- "$cuda_repo_line")
    repo_name="${repo_name%.*}"


    cd "$repo_name"

    if [ "$keep_err_files" -eq 1 ]; then
        echo "----------------- " >> $err_file
        echo "---- errors for repository: $cuda_repo_line ---- " >> $err_file
        echo "----------------- " >> $err_file
    fi

    repo_files_compiled=0
    repo_files_total=0
    repo_files_included=0

    # here we process each line returned by grep, which returns full path of each .cu file
    while read -r cuda_kernel_line; do
        kernel_file_name=$(basename -- "$cuda_kernel_line")
        kernel_file_name="${kernel_file_name%.*}"

        #first, let's try compilation from the top most level directory of the repository
        err_msg=`nvcc -I . -I include -I inc -I common -I Common $cuda_kernel_line -arch=compute_75 -std=c++11 -ptx -o ${repo_files_included}_${kernel_file_name}.ptx 2>&1`

        if [ $? -eq 0 ]; then
            repo_files_compiled=$((repo_files_compiled + 1))

            ptx_file_line_count=`cat ${repo_files_included}_${kernel_file_name}.ptx | wc -l`

            echo -ne "\t\tOK ... line count: $ptx_file_line_count"

            if [ $ptx_file_line_count -gt $line_count_limit ]; then
                repo_files_included=$((repo_files_included + 1))
                echo " ... included"
            else
                rm ${repo_files_included}_${kernel_file_name}.ptx
                echo " ... discarded"
            fi
        else
            echo -ne "\t\tFAIL"
            if [ "$keep_err_files" -eq 1 ]; then
                echo -e "\n*****************" >> $err_file
                echo -e "**** error during compilation for file: $cuda_kernel_line (first try)" >> $err_file
                echo -e "*****************\n" >> $err_file
                echo "$err_msg" >> $err_file
            fi

            #now, let's do a second try, where we try to move one level down in the directory subtree - it is often
            #the case for the repositories, that compilation should be done in this way

            #first, let's actually check, that the path to the kernel is contained in some directory
            echo $cuda_kernel_line | grep -q '/'

            if [ $? -eq 0 ]; then
                #ok, we have a directory in the path and so let's extract it's name
                nested_dir=`echo $cuda_kernel_line | grep -oP --regexp='[^\/]+' | head -n 1`
                
                #now ... we need to check if the nested directory does not contain any spaces
                echo $nested_dir | grep -q ' '

                if [ $? -eq 1 ]; then

                    #now we can change directory into this nested one
                    cd $nested_dir
                    
                    #now we need to count the number of characters in the directory name, so we can remove
                    #this many characters from the beggining of the path; there are several ways how to do this
                    #but throught normal echo and wc -c is the easiest option (it correctly adds +1 to the whole
                    #count, which is convenient for us, as we need to remove also the slash from the path, not only
                    #the directory name ... this really is correct)
                    nested_dir_wc=`echo $nested_dir | wc -c`

                    #and now, to modify the path, we can do just this
                    cuda_kernel_line_stripped="${cuda_kernel_line:nested_dir_wc}"

                    #now the compilation itself
                    err_msg=`nvcc -I . -I include -I inc -I common -I Common $cuda_kernel_line_stripped -arch=compute_75 -std=c++11 -ptx -o ${repo_files_included}_${kernel_file_name}.ptx 2>&1`

                    #and the same thing over again
                    if [ $? -eq 0 ]; then
                        repo_files_compiled=$((repo_files_compiled + 1))

                        ptx_file_line_count=`cat ${repo_files_included}_${kernel_file_name}.ptx | wc -l`

                        echo -ne ", OK ... line count: $ptx_file_line_count"

                        if [ $ptx_file_line_count -gt $line_count_limit ]; then
                            mv ${repo_files_included}_${kernel_file_name}.ptx "../"
                            repo_files_included=$((repo_files_included + 1))
                            echo " ... included"
                        else
                            rm ${repo_files_included}_${kernel_file_name}.ptx
                            echo " ... discarded"
                        fi
                    else
                        echo ", FAIL"
                        if [ "$keep_err_files" -eq 1 ]; then
                            echo -e "\n*****************" >> "../$err_file"
                            echo -e "**** error during compilation for file: $cuda_kernel_line_stripped (second try)" >> "../$err_file"
                            echo -e "*****************\n" >> "../$err_file"
                            echo "$err_msg" >> "../$err_file"
                        fi
                    fi
                
                    cd "../"
                else
                    #path has a directory with spaces in it ... we end
                    echo ", SKIP (nested directory contains spaces)"
                fi

            else
                #path does not contain a directory, we end
                echo ", SKIP (does not contain directory)"
            fi

        fi
        repo_files_total=$((repo_files_total + 1))
    done < <(grep -Rw __global__ --include \*.cu -m 1 | grep -o '.*\.cu')

    echo -e "\t---- repository stats: kernel files total: $repo_files_total, compiled: $repo_files_compiled, included: $repo_files_included"

    # if we actually compiled something ...
    if [ "$repo_files_included" -ne 0 ]; then
        # make the repo directory in the dataset_ptx
        mkdir "../../$dataset_dir/$repo_name"

        # moving all compiled files to the dataset_ptx directory
        mv *.ptx "../../$dataset_dir/$repo_name" 2>/dev/null

        # moving the error report to the dataset_ptx directory
        if [ "$keep_err_files" -eq 1 ]; then
            mv $err_file "../../$dataset_dir/$repo_name" 2>/dev/null
        fi
    fi
    
    cd "../"

    # clean up of this repo
    rm -rf $repo_name

    # and add the processed repository to the processed file
    echo $cuda_repo_line >> "../$processed_repos_file"

    ds_files_total=$((ds_files_total + repo_files_total))
    ds_files_compiled=$((ds_files_compiled + repo_files_compiled))
    ds_files_included=$((ds_files_included + repo_files_included))
    
    echo -e "\nDS stats of this run: kernel files total: $ds_files_total, compiled: $ds_files_compiled, included: $ds_files_included\n" 

done < "../$cuda_repos_file"

echo "done ..."

cd "../"
# remove the tmp dir
rm -rf $dataset_tmp_dir

# finally, remove the empty directories
#cd $dataset_dir
#find . -type d -empty -delete
#cd "../"

# print some stats
echo "Total ptx lines in dataset: $(find . -name '*.ptx' | xargs cat | wc -l)"
